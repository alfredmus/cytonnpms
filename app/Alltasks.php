<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alltasks extends Model
{
    //
    protected $table = 'alltasks';
    protected $fillable = ['taskname','taskcategory','priority_id',
						   'department_id','accesslevel_id','taskdocument',
						   'taskduedate','createdby_id','status_id'
						  ];

}
