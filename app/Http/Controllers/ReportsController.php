<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Status;
use App\Accesslevel;
use App\Departments;
use App\Priority;
use App\Alltasks;
use App\Designation;
use App\UserGroups;
use App\Joblevel;
use Validator;
use App\Staff;
use App\User;
use App\AssignedStaff;
use DB;
use Auth;
use Carbon\Carbon;
use Yajra\Datatables\Facades\Datatables;

class ReportsController extends Controller
{
    //Authenticate all users
    public function __construct()
    {
        $this->middleware('auth');

    }
    //Show all Tasks Reports
      protected function ManagerShowReports(){

        DB::statement(DB::raw('set @rownum=0'));
        $privatetasks = DB::table('alltasks')
        ->join('priority','alltasks.priority_id','=','priority.id')
        ->join('departments','alltasks.department_id','=','departments.id')
        ->join('accessleveltbl','alltasks.accesslevel_id','=','accessleveltbl.id')
        ->join('users', 'alltasks.createdby_id', '=', 'users.id')
        ->join('status', 'alltasks.status_id', '=', 'status.id')
         ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                            'alltasks.id as id',
                            'alltasks.taskname as taskname',
                            'alltasks.taskcategory as taskcategory',
                            'priority.name as priority_id',
                            'departments.name as department_id',
                            'accessleveltbl.name as accesslevel_id',
                            'alltasks.taskdocument as taskdocument',
                            'alltasks.taskduedate as taskduedate',
                            'users.email as createdby_id',
                            'status.name as status_id',
                            DB::raw('DATE(alltasks.created_at) as created_at')
                            ]);
        return Datatables::of($privatetasks)
        ->addColumn('action', function ($privatetasks) {
                return '<div class="btn-group">                           
                            <a href="'.url("adminshowreport/$privatetasks->id").'" type="button" class="btn btn-rounded btn-success"> VIEW REPORT </a>
                        </div>';
            })
        ->rawColumns(['taskdocument','assignuser','action'])
        ->make(true);            
    }

    //Returned all users Assigned to specific task
    protected function managerReports($taskid)
    {
           DB::statement(DB::raw('set @rownum=0'));
            $userAssigned = DB::table('usersassigned')
            ->join('staff','usersassigned.staff_id','=','staff.id')
            ->join('designation','staff.designation_id','=','designation.id')
            ->join('usergroups','staff.usergroup_id','=','usergroups.id') 
            ->join('departments','staff.department','=','departments.id') 
            ->join('joblevel','staff.joblevel','=','joblevel.id') 
            ->join('alltasks','usersassigned.alltasks_id','=','alltasks.id')

            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 
                             'designation.name as designation',
                             'staff.firstname as firstname',
                             'staff.middlename as middlename',
                             'staff.lastname as lastname',
                             'usergroups.name as usertype',
                             'departments.name as department',
                             'joblevel.name as joblevel',
                             'staff.mobilenumber as mobilenumber',
                             'staff.email as email'                            

                    ])
            ->where('alltasks_id', $taskid)
            ->get(); 

            $taskdetails = DB::table('usersassigned')
             ->join('alltasks','usersassigned.alltasks_id','=','alltasks.id')
             ->join('status', 'alltasks.status_id', '=', 'status.id')
             ->select('alltasks.taskdocument as taskdocument',
             		  'alltasks.taskname as taskname',
             		  'alltasks.taskcategory as taskcategory',
             		  'alltasks.taskduedate as taskduedate',
             		  'status.name as status_id'	
         			 )
             ->where('alltasks_id', $taskid)
             ->LIMIT(1)
            ->get();          
              
            return view('admin/taskreport',compact('taskid','userAssigned','taskdetails'));
    }

}
