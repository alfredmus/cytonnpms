<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Status;
use App\UserGroups;
use App\Staff;
use Validator;
use App\User;
use DB;
use Auth;

class ProfileController extends Controller
{
    //All users must be authenticated
    public function __construct()
    {
        $this->middleware('auth');

    }
    //Return admin Profile View
    protected function manProf(){
         $staffid = Auth::User()->email;   
         $getStaff =DB::table('staff')
                   ->join('designation','staff.designation_id','=','designation.id')    
                   ->join('joblevel','staff.joblevel','=','joblevel.id')  
                   ->join('status', 'staff.status_id', '=', 'status.id')
                   ->select(DB::raw('CONCAT(designation.name,
                                     "   ",staff.firstname, "   ",
                                     staff.middlename, "   ", 
                                     staff.lastname) AS fullname'),
                            'staff.mobilenumber as mobilenumber',
                            'joblevel.name as usergroup',
                            'staff.email as email',
                            'staff.staffimage as staffimage',
                            'status.name as status'
                            )
                    ->where('staff.email', '=', $staffid)
                    ->first();        

        return view('admin/profile', compact('getStaff'));
    }

   //Return users Profile View & Data
    protected function userProfile(){
    	 $staffid = Auth::User()->email;   
         $getStaff =DB::table('staff')
         		   ->join('designation','staff.designation_id','=','designation.id')	
                   ->join('joblevel','staff.joblevel','=','joblevel.id')  
                   ->join('status', 'staff.status_id', '=', 'status.id')
                   ->select(DB::raw('CONCAT(designation.name,
                             		 "   ",staff.firstname, "   ",
                             		 staff.middlename, "   ", 
                             		 staff.lastname) AS fullname'),
                     		'staff.mobilenumber as mobilenumber',
                     		'joblevel.name as usergroup',
                     		'staff.email as email',
                     		'staff.staffimage as staffimage',
                     		'status.name as status'
                   			)
                    ->where('staff.email', '=', $staffid)
                    ->first();        

        return view('cytonnusers/profile', compact('getStaff'));
    }

    //Change Users Credentials 
    protected function changCredentials(Request $request){

    	$this->validate($request, ['email'=>'required|exists:users',
    							   'password'=>'required|same:password|min:6|max:12',
    							   'confirmpassword' =>'required|same:password'
    							  ]);
    	$email = $request->email;
    	$password = $request->password;
    	$cryptpassword = bcrypt($password);

        DB::table('users')
            ->where('email', $email)
            ->update(['password' => $cryptpassword]);     

    }
}
