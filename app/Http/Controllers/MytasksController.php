<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Status;
use App\Accesslevel;
use App\Departments;
use App\Priority;
use App\Alltasks;
use App\Designation;
use App\UserGroups;
use App\Joblevel;
use Validator;
use App\Staff;
use App\User;
use App\AssignedStaff;
use DB;
use Auth;
use Carbon\Carbon;
use Yajra\Datatables\Facades\Datatables;

class MytasksController extends Controller
{
    //Authenticate all users
    // public function __construct()
    // {
    //     $this->middleware('auth');

    // }

    //Return Maintenance View
    protected function comingsoon(){

        return view('cytonnusers/comingsoon');
    }

    //Return Task View
    protected function mytasks(){
        $getStatus = Status::all();
        $getAccessLevel = Accesslevel::all();

        DB::statement(DB::raw('set @rownum=0'));
        $privatetasks = DB::table('alltasks')
        ->join('priority','alltasks.priority_id','=','priority.id')
        ->join('departments','alltasks.department_id','=','departments.id')
        ->join('accessleveltbl','alltasks.accesslevel_id','=','accessleveltbl.id')
        ->join('users', 'alltasks.createdby_id', '=', 'users.id')
        ->join('status', 'alltasks.status_id', '=', 'status.id')
         ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                            'alltasks.id as id',
                            'alltasks.taskname as taskname',
                            'alltasks.taskcategory as taskcategory',
                            'priority.name as priority_id',
                            'departments.name as department_id',
                            'accessleveltbl.name as accesslevel_id',
                            'alltasks.taskdocument as taskdocument',
                            'alltasks.taskduedate as taskduedate',
                            'users.email as createdby_id',
                            'status.name as status_id',
                            DB::raw('DATE(alltasks.created_at) as created_at')
                            ])
        ->where('accesslevel_id', 1)
        ->get();

        return view('cytonnusers/mytasks',compact('getStatus','getAccessLevel','privatetasks'));
    }

    //Return Manager Task View
    protected function managerMytasks(){
        $getStatus = Status::all();
        $getAccessLevel = Accesslevel::all();
        return view('admin/mytasks',compact('getStatus','getAccessLevel'));
    }

    //Return Manager Report View
    protected function managerReport(){
        return view('admin/reportlist');
    }

    //Return users View
    protected function user_mgt()
    {
        return view('admin/user_mgt');
    }

    //Return Create new staff View
    protected function addstaff(){

         $getDesignation = Designation::all();
         $getUserGroups = UserGroups::all();
         $getStatus = Status::all();
         $getDepartments = Departments::all();
         $getJoblevel = Joblevel::all();

        return view('admin/add_user', compact('getDesignation', 'getUserGroups','getDepartments','getJoblevel','getStatus'));
    }

    //Create new Staff
    protected function newCytonnStaff(Request $request){

          $this->validate($request, [
                              'designation'=>'required','firstname'=>'required',
                              'middlename'=>'required','lastname'=>'required',
                              'department'=>'required','joblevel'=>'required',
                              'usertype'=>'required',
                              'mobilenumber'=>'required|min:12|max:12|regex:/(254)[0-9]{9}/|unique:staff',
                              'email'=>'required|email|unique:staff',
                              'status'=>'required'

                            ]);

    $newS = Staff::create(['designation_id'=>$request->designation,
                           'firstname'=>$request->firstname,
                           'middlename'=>$request->middlename,
                           'lastname'=>$request->lastname,
                           'usergroup_id'=>$request->usertype,
                           'mobilenumber'=>$request->mobilenumber,
                           'email'=>$request->email,
                           'department'=>$request->department,
                           'joblevel'=>$request->joblevel,
                           'status_id'=>$request->status,
                           'createdby_id'=>Auth::User()->id,
                          
                          ]);
    $too = $request->firstname;
    $portallink1 = 'Test.gateway42.com'; 
    $plainpassword = str_random(8);
    $msm = [
            'to'=>$too,'portallink'=>$portallink1,
            'Username'=>$request->email,'Password'=>$plainpassword
            ];  

    $messageBody = $msm;
    $usertypeid = $request->usertype;    
    $cryptpassword = bcrypt($plainpassword);  

    $usertype = DB::table('usergroups')->select('name')
                ->where('id','=',$usertypeid)
                ->get();
                foreach($usertype as $key){
                    $tired = $key->name;                     
                } 

                if ($tired == 'Department manager') {
                	$admintype = 1;
                }
                else{
                	$admintype = 0;
                }

    $cyptUser = User::create(['name'=>$request->firstname,
                               'email'=>$request->email,
                               'usertype'=>$tired,
                               'is_admin'=>$admintype,
                               'statusid'=>$request->status,
                               'password'=>$cryptpassword
                              ]); 

    //Send  Password Email to New Staff created account
    Mail::send(['html' => 'layouts.mail'],$msm,    
         function ($message) use($request)
            {
                $message->from('no-reply@cytonn.com', "Cytonn Pms Platform");
                $message->subject("Welcome to our Task & Follow Up Portal ");
                $message->to($request->email);
            });                      

    }

    //Show All Staff
     protected function showCytonnStaff(){

        DB::statement(DB::raw('set @rownum=0'));
            $showS = DB::table('staff')
            ->join('designation','staff.designation_id','=','designation.id')
            ->join('usergroups','staff.usergroup_id','=','usergroups.id') 
			->join('departments','staff.department','=','departments.id') 
			->join('joblevel','staff.joblevel','=','joblevel.id') 
            ->join('users','staff.createdby_id','=','users.id')  
            ->join('status', 'staff.status_id', '=', 'status.id')
            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 

                     'designation.name as designation',
                     'staff.firstname as firstname',
                     'staff.middlename as middlename',
                     'staff.lastname as lastname',
                     'usergroups.name as usertype',
                     'departments.name as department',
                     'joblevel.name as joblevel',
                     'staff.mobilenumber as mobilenumber',
                     'staff.email as email',
                     'users.email as createdby_id',
                     'status.name as status',
                     DB::raw('DATE(staff.created_at) as created_at'),  
                     'staff.updated_at as updated_at'      
                    ]);
             return Datatables::of($showS)
             ->addColumn('action', function ($user) {
                return '<div class="btn-group">                            
                            <button type="button" class="btn btn-rounded btn-success" data-id=" {{ $id }}">
                            EDIT </button>
                        </div>';
            })

            ->make(true);

    }


    //Show Members Create new Task view
    protected function newtask(){

    	$getStatus = Status::all();
    	$getAccessLevel = Accesslevel::all();
    	$getDepartments = Departments::all();
    	$getPriority = Priority::all();
        return view('cytonnusers/createnewtask',compact('getStatus','getAccessLevel','getDepartments','getPriority'));
    }

    //Show Manager Create new Task view
    protected function managerNewtask(){

        $getStatus = Status::all();
        $getAccessLevel = Accesslevel::all();
        $getDepartments = Departments::all();
        $getPriority = Priority::all();
        return view('admin/createnewtask',compact('getStatus','getAccessLevel','getDepartments','getPriority'));
    }

    //Managers Create newtask
    protected function managerCreateNewtask(Request $request){

        $this->validate($request, ['taskname'=>'required|unique:alltasks',
                                   'taskcategory'=>'required',
                                   'priority'=>'required',
                                   'departments'=>'required',
                                   'accesslevel'=>'required',
                                   'taskdocument'=>'mimes:jpeg,png,jpg|max:2048',
                                   'duedate'=>'required',
                                   'status'=>'required'
                                  ]);


            if ($request->hasFile('taskdocument')) {
                        $file = $request->taskdocument;
                        $taskname = $request->taskname;           
                        $tmpFilePath = '/TaskDocuments/'.$taskname;
                        $tmpFileName = $file->getClientOriginalName();
                        $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
                        $taskdocument = $tmpFilePath.'/'.$tmpFileName;
                }

            else{
                $taskdocument = '';
             }

       $ntask = Alltasks::create(['taskname'=>$request->taskname,
                                   'taskcategory'=>$request->taskcategory,
                                   'priority_id'=>$request->priority,
                                   'department_id'=>$request->departments,
                                   'accesslevel_id'=>$request->accesslevel,
                                   'taskdocument'=>$taskdocument,
                                   'taskduedate'=>$request->duedate,                                   
                                   'createdby_id'=>Auth::User()->id,
                                   'status_id'=>$request->status                          
                                ]);

       return Redirect::to('managermytasks')->with('success','Task created successfully!');
    }

    //Members Create newtask
    protected function createnewtask(Request $request){

    	$this->validate($request, ['taskname'=>'required|unique:alltasks',
    							   'taskcategory'=>'required',
    							   'priority'=>'required',
						   		   'departments'=>'required',
						   		   'accesslevel'=>'required',
						   		   'taskdocument'=>'mimes:jpeg,png,jpg|max:2048',
						   		   'duedate'=>'required',
						   		   'status'=>'required'
    							  ]);


	    	if ($request->hasFile('taskdocument')) {
	                    $file = $request->taskdocument;
	                    $taskname = $request->taskname;           
	                    $tmpFilePath = '/TaskDocuments/'.$taskname;
	                    $tmpFileName = $file->getClientOriginalName();
	                    $file = $file->move(public_path() . $tmpFilePath, $tmpFileName);
	                    $taskdocument = $tmpFilePath.'/'.$tmpFileName;
	            }

            else{
                $taskdocument = '';
             }

       $ntask = Alltasks::create(['taskname'=>$request->taskname,
		                           'taskcategory'=>$request->taskcategory,
		                           'priority_id'=>$request->priority,
		                           'department_id'=>$request->departments,
		                           'accesslevel_id'=>$request->accesslevel,
		                           'taskdocument'=>$taskdocument,
		                           'taskduedate'=>$request->duedate,		                           
		                           'createdby_id'=>Auth::User()->id,
		                           'status_id'=>$request->status                          
                          		]);

       return Redirect::to('mytasks')->with('success','Task created successfully!');
    }

    //Show all Private Tasks to managers
      protected function ManagerShowPrivateTasks(){

        DB::statement(DB::raw('set @rownum=0'));
        $privatetasks = DB::table('alltasks')
        ->join('priority','alltasks.priority_id','=','priority.id')
        ->join('departments','alltasks.department_id','=','departments.id')
        ->join('accessleveltbl','alltasks.accesslevel_id','=','accessleveltbl.id')
        ->join('users', 'alltasks.createdby_id', '=', 'users.id')
        ->join('status', 'alltasks.status_id', '=', 'status.id')
         ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                            'alltasks.id as id',
                            'alltasks.taskname as taskname',
                            'alltasks.taskcategory as taskcategory',
                            'priority.name as priority_id',
                            'departments.name as department_id',
                            'accessleveltbl.name as accesslevel_id',
                            'alltasks.taskdocument as taskdocument',
                            'alltasks.taskduedate as taskduedate',
                            'users.email as createdby_id',
                            'status.name as status_id',
                            DB::raw('DATE(alltasks.created_at) as created_at')
                            ]);
        return Datatables::of($privatetasks)
        ->addColumn('taskdocument', function ($privatetasks) {
                return '<button type="button" class="btn btn-rounded btn-default" data-toggle="modal" data-target="#'.$privatetasks->id.'">Preview '.$privatetasks->taskname.' Document <i class="icon-play3 position-right"></i></button>
                   <div id="'.$privatetasks->id.'" class="modal fade">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">'.$privatetasks->department_id.' Department</h5>
                                </div>

                                <div class="modal-body">
                                     <img src="/cytonnpms/public'.$privatetasks->taskdocument.'">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>';
            })
          ->addColumn('assignuser', function ($privatetasks) {
                return '<div class="btn-group">                           
                            <a href="'.url("adminshowtaskuser/$privatetasks->id").'" type="button" class="btn btn-rounded btn-default openModal"> ASSIGN TASK USER </a>
                        </div>';         
            })
        ->addColumn('action', function ($privatetasks) {
                return '<div class="btn-group">                            
                            <button type="button" class="btn btn-rounded btn-success openModal"  data-id="'.$privatetasks->id.'" data-toggle="modal" data-target="#edittask">
                            EDIT </button>
                        </div>';
            })
        ->rawColumns(['taskdocument','assignuser','action'])
        ->make(true);            
    }

    //Show all Public Tasks to Members
      protected function showPrivateTasks(){

        DB::statement(DB::raw('set @rownum=0'));
        $privatetasks = DB::table('alltasks')
        ->join('priority','alltasks.priority_id','=','priority.id')
        ->join('departments','alltasks.department_id','=','departments.id')
        ->join('accessleveltbl','alltasks.accesslevel_id','=','accessleveltbl.id')
        ->join('users', 'alltasks.createdby_id', '=', 'users.id')
        ->join('status', 'alltasks.status_id', '=', 'status.id')
         ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                            'alltasks.id as id',
                            'alltasks.taskname as taskname',
                            'alltasks.taskcategory as taskcategory',
                            'priority.name as priority_id',
                            'departments.name as department_id',
                            'accessleveltbl.name as accesslevel_id',
                            'alltasks.taskdocument as taskdocument',
                            'alltasks.taskduedate as taskduedate',
                            'users.email as createdby_id',
                            'status.name as status_id',
                            DB::raw('DATE(alltasks.created_at) as created_at')
                            ]);
        return Datatables::of($privatetasks)
        ->addColumn('taskdocument', function ($privatetasks) {
                return '<button type="button" class="btn btn-rounded btn-default" data-toggle="modal" data-target="#'.$privatetasks->id.'">Preview '.$privatetasks->taskname.' Document <i class="icon-play3 position-right"></i></button>
                   <div id="'.$privatetasks->id.'" class="modal fade">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h5 class="modal-title">'.$privatetasks->department_id.' Department</h5>
                                </div>

                                <div class="modal-body">
                                     <img src="/cytonnpms/public'.$privatetasks->taskdocument.'">
                                </div>

                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>';
            })
          ->addColumn('assignuser', function ($privatetasks) {
                return '<div class="btn-group">                           
                            <a href="'.url("showtaskuser/$privatetasks->id").'" type="button" class="btn btn-rounded btn-default openModal"> ASSIGN TASK USER </a>
                        </div>';         
            })
        ->addColumn('action', function ($privatetasks) {
                return '<div class="btn-group">                            
                            <button type="button" class="btn btn-rounded btn-success openModal"  data-id="'.$privatetasks->id.'" data-toggle="modal" data-target="#edittask">
                            EDIT </button>
                        </div>';
            })
        ->rawColumns(['taskdocument','assignuser','action'])
        ->make(true);            
    }

    //Return managers settings view
    protected function admsettings()
    {
        return view('admin/settings');
    }

    //Return members settings view
    protected function settings()
    {
        return view('cytonnusers/settings');
    }

    //Returned all users Assigned to specific task
    protected function managerTaskuser($taskid)
    {
           DB::statement(DB::raw('set @rownum=0'));
            $userAssigned = DB::table('usersassigned')
            ->join('staff','usersassigned.staff_id','=','staff.id')
            ->join('designation','staff.designation_id','=','designation.id')
            ->join('usergroups','staff.usergroup_id','=','usergroups.id') 
            ->join('departments','staff.department','=','departments.id') 
            ->join('joblevel','staff.joblevel','=','joblevel.id') 
            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 
                             'designation.name as designation',
                             'staff.firstname as firstname',
                             'staff.middlename as middlename',
                             'staff.lastname as lastname',
                             'usergroups.name as usertype',
                             'departments.name as department',
                             'joblevel.name as joblevel',
                             'staff.mobilenumber as mobilenumber',
                             'staff.email as email'      
                    ])
            ->where('alltasks_id', $taskid)
            ->get();
            return view('admin/taskusers',compact('taskid','userAssigned'));
    }

    //Cytonn Members view all users Assigned to specific task
    protected function taskuser($taskid)
    {
           DB::statement(DB::raw('set @rownum=0'));
            $userAssigned = DB::table('usersassigned')
            ->join('staff','usersassigned.staff_id','=','staff.id')
            ->join('designation','staff.designation_id','=','designation.id')
            ->join('usergroups','staff.usergroup_id','=','usergroups.id') 
            ->join('departments','staff.department','=','departments.id') 
            ->join('joblevel','staff.joblevel','=','joblevel.id') 
            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 
                             'designation.name as designation',
                             'staff.firstname as firstname',
                             'staff.middlename as middlename',
                             'staff.lastname as lastname',
                             'usergroups.name as usertype',
                             'departments.name as department',
                             'joblevel.name as joblevel',
                             'staff.mobilenumber as mobilenumber',
                             'staff.email as email'      
                    ])
            ->where('alltasks_id', $taskid)
            ->get();
            return view('cytonnusers/taskusers',compact('taskid','userAssigned'));
    }


    protected function getUsersPerTask($id){

        DB::statement(DB::raw('set @rownum=0'));
            $userAssigned = DB::table('usersassigned')
            ->join('staff','usersassigned.staff_id','=','staff.id')
            ->join('designation','staff.designation_id','=','designation.id')
            ->join('usergroups','staff.usergroup_id','=','usergroups.id') 
			->join('departments','staff.department','=','departments.id') 
			->join('joblevel','staff.joblevel','=','joblevel.id') 
            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 
		                     'designation.name as designation',
		                     'staff.firstname as firstname',
		                     'staff.middlename as middlename',
		                     'staff.lastname as lastname',
		                     'usergroups.name as usertype',
		                     'departments.name as department',
		                     'joblevel.name as joblevel',
		                     'staff.mobilenumber as mobilenumber',
		                     'staff.email as email'      
                    ])
            ->where('alltasks_id', $id)
                    ->get();

            	return $userAssigned;
        
    }
        //Return users to assign to selected task
       protected function selectCytonnStaff(){
        DB::statement(DB::raw('set @rownum=0'));
            $selectStaff = DB::table('staff')
            ->join('designation','staff.designation_id','=','designation.id')
			->join('departments','staff.department','=','departments.id') 
			->join('joblevel','staff.joblevel','=','joblevel.id') 
            ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 
            		 'staff.id as id',
                     'designation.name as designation',
                     'staff.firstname as firstname',
                     'staff.middlename as middlename',
                     'staff.lastname as lastname',
                     'departments.name as department',
                     'joblevel.name as joblevel',
                     'staff.mobilenumber as mobilenumber'     
                    ]);
             return Datatables::of($selectStaff)
             ->addColumn('action', function ($selectStaff) {
                return '<div class="btn-group">                            
                            <button type="button" class="btn btn-rounded btn-success selectTaskStaff" data-id="'.$selectStaff->id.'">
                            Select </button>
                        </div>';
            })
            ->make(true);

    }

	    //save assigned user and notify via email
	    protected function assignStaff(Request $request){
	    	 $assgtask = AssignedStaff::create(['alltasks_id'=>$request->alltasks_id,
		                           				'staff_id'=>$request->staff_id                          
                          					  ]);
        //get user details     
            $tired = $request->staff_id ;
            $user = DB::table('staff')
            ->select(
                     'firstname',
                     'email'                            
                    )
            ->where('id', $tired)
            ->get();

            foreach($user as $key){
                $username = $key->firstname; 
                $useremail = $key->email;                     
            } 
               
           $msm = ['to'=>$useremail, 'fname'=>$username ];             

    //Send  Notification Email to all assigned Staff members
    Mail::send(['html' => 'layouts.newtasknotification'],$msm,    
         function ($message) use($useremail)
            {
                $message->from('no-reply@cytonn.com', "Cytonn Pms Platform");
                $message->subject("Welcome to our Task & Follow Up Portal ");
                $message->to($useremail);
            });
    }

        //Find task to edit
        protected function findTaskById($id){
            DB::statement(DB::raw('set @rownum=0'));
            $edittasks = DB::table('alltasks')
            ->join('priority','alltasks.priority_id','=','priority.id')
            ->join('departments','alltasks.department_id','=','departments.id')
            ->join('accessleveltbl','alltasks.accesslevel_id','=','accessleveltbl.id')
            ->join('status', 'alltasks.status_id', '=', 'status.id')
             ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                                'alltasks.id as id',
                                'alltasks.taskname as taskname',
                                'alltasks.taskcategory as taskcategory',
                                'accessleveltbl.name as accesslevel_id',
                                'status.name as status_id'
                                ])
            ->where('alltasks.id', $id)
            ->get();

            return $edittasks;
        }

    //Show Tasks assigned to manager
    protected function findTaskPerUser(){
        $getStatus = Status::all();
        $getAccessLevel = Accesslevel::all();

        $id = Auth::User()->id;
            //$id = 18;
            DB::statement(DB::raw('set @rownum=0'));
            $ttasks = DB::table('usersassigned')
            ->join('alltasks','usersassigned.alltasks_id','=','alltasks.id')
            ->join('priority','alltasks.priority_id','=','priority.id')
            ->join('departments','alltasks.department_id','=','departments.id')
            ->join('accessleveltbl','alltasks.accesslevel_id','=','accessleveltbl.id')
            ->join('users', 'alltasks.createdby_id', '=', 'users.id')
            ->join('status', 'alltasks.status_id', '=', 'status.id')
             ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                                'alltasks.id as id',
                                'alltasks.taskname as taskname',
                                'alltasks.taskcategory as taskcategory',
                                'priority.name as priority_id',
                                'departments.name as department_id',
                                'accessleveltbl.name as accesslevel_id',
                                'alltasks.taskdocument as taskdocument',
                                'alltasks.taskduedate as taskduedate',
                                'users.email as createdby_id',
                                'status.name as status_id',
                                DB::raw('DATE(alltasks.created_at) as created_at')
                                ])
                ->where('staff_id', $id)
                ->get();
        return view('admin/assignedtask',compact('getStatus','getAccessLevel','ttasks'));
    }

    //Show Tasks assigned to Members
    protected function usersFindTaskPerUser(){
        $getStatus = Status::all();
        $getAccessLevel = Accesslevel::all();

        $id = Auth::User()->id;
            //$id = 18;
            DB::statement(DB::raw('set @rownum=0'));
            $ttasks = DB::table('usersassigned')
            ->join('alltasks','usersassigned.alltasks_id','=','alltasks.id')
            ->join('priority','alltasks.priority_id','=','priority.id')
            ->join('departments','alltasks.department_id','=','departments.id')
            ->join('accessleveltbl','alltasks.accesslevel_id','=','accessleveltbl.id')
            ->join('users', 'alltasks.createdby_id', '=', 'users.id')
            ->join('status', 'alltasks.status_id', '=', 'status.id')
             ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                                'alltasks.id as id',
                                'alltasks.taskname as taskname',
                                'alltasks.taskcategory as taskcategory',
                                'priority.name as priority_id',
                                'departments.name as department_id',
                                'accessleveltbl.name as accesslevel_id',
                                'alltasks.taskdocument as taskdocument',
                                'alltasks.taskduedate as taskduedate',
                                'users.email as createdby_id',
                                'status.name as status_id',
                                DB::raw('DATE(alltasks.created_at) as created_at')
                                ])
                ->where('staff_id', $id)
                ->get();
        return view('cytonnusers/userassignedtasks',compact('getStatus','getAccessLevel','ttasks'));
    }


    //Find task per user
        protected function findTaskPerUserAssigned(){
            $id = Auth::User()->id;
            //$id = 18;
            DB::statement(DB::raw('set @rownum=0'));
            $ttasks = DB::table('usersassigned')
            ->join('alltasks','usersassigned.alltasks_id','=','alltasks.id')
            ->join('priority','alltasks.priority_id','=','priority.id')
            ->join('departments','alltasks.department_id','=','departments.id')
            ->join('accessleveltbl','alltasks.accesslevel_id','=','accessleveltbl.id')
            ->join('users', 'alltasks.createdby_id', '=', 'users.id')
            ->join('status', 'alltasks.status_id', '=', 'status.id')
             ->select([DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                                'alltasks.id as id',
                                'alltasks.taskname as taskname',
                                'alltasks.taskcategory as taskcategory',
                                'priority.name as priority_id',
                                'departments.name as department_id',
                                'accessleveltbl.name as accesslevel_id',
                                'alltasks.taskdocument as taskdocument',
                                'alltasks.taskduedate as taskduedate',
                                'users.email as createdby_id',
                                'status.name as status_id',
                                DB::raw('DATE(alltasks.created_at) as created_at')
                                ])
                ->where('staff_id', $id)
                ->get();

                return $ttasks;
        }

        //find user to edit
        protected function findUserToEdit($id){

        $editClient = DB::table('clients')
                    ->join('designation','clients.designation_id','=','designation.id')
                    ->join('clientgroups','clients.clientgroup_id','=','clientgroups.id')
                    ->join('status','clients.status_id','=','status.id')
                    ->select('clients.accountnumber as accountnumber',
                             'designation.name as designation_id',
                             'clientgroups.name as clientgroup_id',
                             'clients.firstname as firstname',
                             'clients.middlename as middlename',
                             'clients.lastname as lastname',
                             'clients.bankaccount as bankaccount',
                             'clients.nationalidno as nationalidno',
                             'clients.mobilenumber as mobilenumber',
                             'clients.email as email',
                             'clients.postaladdress as postaladdress',
                             'status.name as status_id')
                    ->where('accountnumber', $id)
                    ->get();

        return $editClient;
    }
    //update Task and Notify tagged users
    protected function updateTasks(Request $request){

        $this->validate($request, ['editaccesslevel'=>'required',
                                   'editstatus'=>'required',
                                   'edittaskname' =>'required'
                                  ]);
        $editaccesslevel = $request->editaccesslevel;
        $editstatus = $request->editstatus;
        $edittaskname = $request->edittaskname;

        DB::table('alltasks')
            ->where('taskname', $edittaskname)
            ->update(['accesslevel_id' => $editaccesslevel,
                      'status_id' => $editstatus,
                    ]); 
 
   //Find all users assigned to the specified task
        $edittaskname = $request->edittaskname;
        $taskid = DB::table('alltasks')->select('id')
                    ->where('taskname','=',$edittaskname)
                    ->get();

            foreach($taskid as $key){
                $tired = $key->id;                     
            } 
            
         $user = DB::table('usersassigned')
            ->join('staff','usersassigned.staff_id','=','staff.id')
            ->select(
                     'staff.firstname as firstname',
                     'staff.email as email'                            
                    )
            ->where('alltasks_id', $tired)
            ->get();

            foreach($user as $key){
                $username = $key->firstname; 
                $useremail = $key->email;                     
            //} 
            
            $msm = [
                    'to'=>$username,
                    'taskname'=>$request->edittaskname,
                    ];             

    //Send  Notification Email to all assigned Staff members
    Mail::send(['html' => 'layouts.emailnotification'],$msm,    
         function ($message) use($useremail)
            {
                $message->from('no-reply@cytonn.com', "Cytonn Pms Platform");
                $message->subject("Welcome to our Task & Follow Up Portal ");
                $message->to($useremail);
            });
        }
    }
    //fetch dashboard data
       protected function showDashboard(){
            //Fetch ongoing tasks only
              $ongoingT = DB::table('alltasks')
                ->select(DB::raw('count(*) as Ongoing'))
                ->where('status_id', 1)
                ->get();

                foreach ($ongoingT as $key) {
                    $ongoing = $key->Ongoing;
                }

            //Fetch complete tasks only
              $closedT = DB::table('alltasks')
                ->select(DB::raw('count(*) as Closed'))
                ->where('status_id', 5)
                ->get();

                foreach ($closedT as $key) {
                    $closed = $key->Closed;
                }

            //Fetch Public tasks only
              $publicT = DB::table('alltasks')
                ->select(DB::raw('count(*) as Public'))
                ->where('accesslevel_id', 1)
                ->get();

                foreach ($publicT as $key) {
                    $public = $key->Public;
                }

            //Fetch Private tasks only
              $privateT = DB::table('alltasks')
                ->select(DB::raw('count(*) as Private'))
                ->where('accesslevel_id', 2)
                ->get();

                foreach ($privateT as $key) {
                    $private = $key->Private;
                }

        return view('cytonnusers/dashboard',compact('ongoing','closed',
                    'public','private'));

      }

}
