<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //Authenticate all users
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    //Return Managers dashboard
    protected function manager_dashboard()
    {
        //Fetch ongoing tasks only
              $ongoingT = DB::table('alltasks')
                ->select(DB::raw('count(*) as Ongoing'))
                ->where('status_id', 1)
                ->get();

                foreach ($ongoingT as $key) {
                    $ongoing = $key->Ongoing;
                }

            //Fetch complete tasks only
              $closedT = DB::table('alltasks')
                ->select(DB::raw('count(*) as Closed'))
                ->where('status_id', 5)
                ->get();

                foreach ($closedT as $key) {
                    $closed = $key->Closed;
                }

            //Fetch Public tasks only
              $publicT = DB::table('alltasks')
                ->select(DB::raw('count(*) as Public'))
                ->where('accesslevel_id', 1)
                ->get();

                foreach ($publicT as $key) {
                    $public = $key->Public;
                }

            //Fetch Private tasks only
              $privateT = DB::table('alltasks')
                ->select(DB::raw('count(*) as Private'))
                ->where('accesslevel_id', 2)
                ->get();

                foreach ($privateT as $key) {
                    $private = $key->Private;
                }

        return view('admin/dashboard',compact('ongoing','closed',
                    'public','private'));
    }
   
   //Return users view
    protected function user_mgt()
    {
        return view('admin/user_mgt');
    }

    //Return clients dashboard
    protected function admin_clientmaster()
    {
        return view('admin/clientmaster');
    }
}
