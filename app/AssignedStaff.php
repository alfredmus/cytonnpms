<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AssignedStaff extends Model
{
    //
    protected $table = 'usersassigned';
    protected $fillable = ['alltasks_id','staff_id'];
}
