<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staff';
    protected $fillable = ['designation_id','firstname','middlename',
						   'lastname','usergroup_id','email','department','joblevel',
						   'mobilenumber','status_id',
						   'createdby_id'	
							];
}
