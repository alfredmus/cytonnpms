<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'prevent-back-history'],function(){
Auth::routes();
Route::get('/', function () {
    return view('auth/login');
});

//cytonn Managers routes
Route::get('manager_dashboard','HomeController@manager_dashboard');
Route::get('adminsettings', 'MytasksController@admsettings');
Route::get('managerprofile', 'ProfileController@manProf');
Route::get('manageruserboard', 'MytasksController@managerMytasks');
Route::get('adminnewtask', 'MytasksController@managerNewtask');
Route::post('adminaddnewtask', 'MytasksController@managerCreateNewtask');
Route::get('adminshowtaskuser/{taskid}', 'MytasksController@managerTaskuser');
Route::get('adminshowprivatetasks', 'MytasksController@ManagerShowPrivateTasks');
Route::get('findTaskPerUser', 'MytasksController@findTaskPerUser');
Route::get('findTaskPerUserAssigned', 'MytasksController@findTaskPerUserAssigned');
Route::get('managerReport', 'MytasksController@managerReport');
Route::get('ManagerShowReports', 'ReportsController@ManagerShowReports');
Route::get('adminshowreport/{taskid}', 'ReportsController@managerReports');


//cytonn Members routes
Route::get('editClient/{id}','MytasksController@findUserToEdit');
Route::post('updateusercredentials','ProfileController@changCredentials');
Route::get('admin_dashboard','MytasksController@showDashboard');
Route::get('profile', 'ProfileController@userProfile');
Route::get('mytasks', 'MytasksController@mytasks');
Route::get('newtask', 'MytasksController@newtask');
Route::post('addnewtask', 'MytasksController@createnewtask');
Route::get('comingsoon', 'MytasksController@comingsoon');
Route::get('showprivatetasks', 'MytasksController@showPrivateTasks');
Route::get('settings', 'MytasksController@settings');
Route::get('user_mgt', 'MytasksController@user_mgt');
Route::get('add_user', 'MytasksController@addstaff');
Route::post('createnewcytonnstaff','MytasksController@newCytonnStaff');
Route::get('showallcytonnstaff', 'MytasksController@showCytonnStaff');
Route::get('selectCytonnStaff', 'MytasksController@selectCytonnStaff');
Route::get('showtaskuser/{taskid}', 'MytasksController@taskuser');
Route::get('getUsersPerTask/{id}', 'MytasksController@getUsersPerTask');
Route::post('assigntaskstaff','MytasksController@assignStaff');
Route::get('findTaskById/{id}','MytasksController@findTaskById');
Route::get('support','HomeController@support');
Route::get('usersTasks', 'MytasksController@usersFindTaskPerUser');

Route::post('updateTasks', 'MytasksController@updateTasks');
//Route::get('dashboarddata', 'MytasksController@showDashboard');

});

