<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',30);
            $table->string('email',40)->unique();
            $table->string('usertype',40);
            $table->integer('is_admin')->unsigned();
            $table->string('password');
            $table->integer('statusid')->unsigned();
            $table->foreign('statusid')->references('id')->on('status');
            $table->rememberToken();
            $table->timestamps();      

           


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
