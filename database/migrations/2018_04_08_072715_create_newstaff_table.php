<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewstaffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('designation_id')->unsigned()->length(2);      
            $table->string('firstname',16);
            $table->string('middlename',16);
            $table->string('lastname',16);
            $table->integer('mobilenumber')->unique()->unsigned()->length(12);
            $table->string('email',40)->unique();
            $table->string('staffimage',120)->unique();
            $table->integer('usergroup_id')->unsigned()->length(2);
            $table->integer('createdby_id')->unsigned()->length(2);
            $table->integer('status_id')->unsigned()->length(2);
            $table->timestamps();

            $table->foreign('designation_id')
                  ->references('id')->on('designation')
                  ->onDelete('cascade');          

            $table->foreign('usergroup_id')
                  ->references('id')->on('usergroups')
                  ->onDelete('cascade');

            $table->foreign('createdby_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');

            $table->foreign('status_id')
                  ->references('id')->on('status')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff');
    }
}
