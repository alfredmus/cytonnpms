<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersassignedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usersassigned', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('alltasks_id')->unsigned()->length(2);
            $table->integer('staff_id')->unsigned()->length(2);
            $table->timestamps();

            $table->foreign('alltasks_id')
              ->references('id')->on('alltasks')
              ->onDelete('cascade');

              
            $table->foreign('staff_id')
              ->references('id')->on('staff')
              ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usersassigned');
    }
}
