<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAlltasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alltasks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('taskname',40);
            $table->string('taskcategory',40);
            $table->integer('priority_id')->unsigned()->length(2);            
            $table->integer('department_id')->unsigned()->length(2);
            $table->integer('accesslevel_id')->unsigned()->length(2);
            $table->string('krapinimage',120);
            $table->string('duedate',30);
            $table->integer('createdby_id')->unsigned()->length(2);            
            $table->integer('status_id')->unsigned()->length(2);
            $table->timestamps();

            $table->foreign('createdby_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');

            $table->foreign('status_id')
                  ->references('id')->on('status')
                  ->onDelete('cascade');

            $table->foreign('priority_id')
                  ->references('id')->on('priority')
                  ->onDelete('cascade');

            $table->foreign('department_id')
                  ->references('id')->on('departments')
                  ->onDelete('cascade');

            $table->foreign('accesslevel_id')
                  ->references('id')->on('accessleveltbl')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alltasks');
    }
}
