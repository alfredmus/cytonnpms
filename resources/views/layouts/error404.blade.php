@extends('admin.header')
@section('content')

	<!-- Page container -->
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content">

					<div class="text-center content-group">
						<h1 class="error-title" style="color: #1B2631">IBS</h1>
						<h2><i style="color: #B03A2E ">We are sorry, your not Authorised to view this Page!</i></h2>
					</div>
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
				<div class="text-center">			
					<a href="{{ route('logout') }}" class="btn border-slate text-slate-800 btn-flat"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            <i class="icon-switch2"></i> 
                            <span>Back to Login</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
				</div>				
						</div>
					</div>
					 @include('client.footer')  

				</div>
			</div>
		</div>
	</div>
  @endsection
