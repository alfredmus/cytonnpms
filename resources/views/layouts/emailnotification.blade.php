<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<title>Cytonn PMS</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0 " />
<meta name="format-detection" content="telephone=no" />


<style type="text/css">
body {
    
    -webkit-text-size-adjust: 100% !important;
    -ms-text-size-adjust: 100% !important;
    -webkit-font-smoothing: antialiased !important;
}
img {
    border: 0 !important;
    outline: none !important;
}
p {
    Margin: 0px !important;
    Padding: 0px !important;
}
table {
    border-collapse: collapse;
    mso-table-lspace: 0px;
    mso-table-rspace: 0px;
}
td, a, span {
    border-collapse: collapse;
    mso-line-height-rule: exactly;
}
.ExternalClass * {
    line-height: 100%;
}
span.MsoHyperlink {
      mso-style-priority:99;
      color:inherit;}
  
span.MsoHyperlinkFollowed {
      mso-style-priority:99;
      color:inherit;}

.em_headertext_grey {
    color: #333333;
    font-family:Arial, sans-serif;
    font-size: 12px;
    line-height: 20px;
    text-decoration:none;
        
}
.em_headertext_grey a, .em_prizetext2_grey span {
    color: #f06060;
    text-decoration:none;
    white-space:nowrap; 
}
.em_bodytext_white {
    color: #ffffff;
    font-family:Arial, sans-serif;
    font-size: 14px;
    line-height: 18px;
    text-decoration:none;
    
}
.em_bodytext_white a {
    color: #ffffff;
    text-decoration:underline;  
}
.em_nevigationtext_white {
    color: #ffffff;
    font-family:Arial, sans-serif;
    font-size: 14px;
    line-height: 17px;
    text-decoration:none;
    
}
.em_nevigationtext_white a {
    color: #ffffff;
    text-decoration:none;   
}
.em_titletext_black {
    color: #333333;
    font-family:Arial, sans-serif;
    font-size: 24px;
    line-height: 27px;
    text-decoration:none;
        
}
.em_subtitletext_black {
    color: #333333;
    font-family:Arial, sans-serif;
    font-size: 14px;
    line-height: 17px;
    text-decoration:none;   
}

.em_headingtext_black {
    color: #333333;
    font-family:Arial, sans-serif;
    font-size: 18px;
    line-height: 24px;
    text-decoration:none;
    
    
}
.em_bodytext_grey {
    color: #808080;
    font-family:Arial, sans-serif;
    font-size: 13px;
    line-height: 20px;
    text-decoration:none;
    
    
}
.em_bodytext_grey a {
    color: #f06060;
    text-decoration:none;
    white-space:nowrap; 
}

.em_titletext_white {
    color: #ffffff;
    font-family:Arial, sans-serif;
    font-size: 24px;
    line-height: 27px;
    text-decoration:none;
    
}

.em_headingtext_white {
    color: #ffffff;
    font-family:Arial, sans-serif;
    font-size: 20px;
    line-height: 22px;
    text-decoration:none;
    
}

.em_footertext_grey {
    color:  #ffffff;
    font-family:Arial, sans-serif;
    font-size: 12px;
    line-height: 20px;
    text-decoration:none;
        
}
.em_footertext_grey a {
    color: #f06060;
    text-decoration:underline;
    white-space:nowrap; 
}
.em_mrp_text {
    color: #808080;
    font-family:Arial, sans-serif;
    font-size: 14px;
    line-height: 20px;
    text-decoration:line-through;
    
}

.em_prizetext2_grey {
    color: #333333;
    font-family:Arial, sans-serif;
    font-size: 18px;
    line-height: 20px;
    text-decoration:none;
    
}

.em_contact_text1_grey {
    color: #f1f4f5;
    font-family:Arial, sans-serif;
    font-size: 18px;
    line-height: 20px;
    text-decoration:none;
    font-weight:bold;
    
}

.em_contact_text2_grey {
    color: #808080;
    font-family:Arial, sans-serif;
    font-size: 12px;
    line-height: 22px;
    text-decoration:none;   
}
.em_contact_text2_grey a {
    color: #808080;
     text-decoration:none;  
}

.em_button_text_white {
    color: #ffffff;
    font-family:Arial, sans-serif;
    font-size: 16px;
   text-decoration:none;
    
    
}
.em_button_text_white a {
    color: #ffffff;
     text-decoration:none;
     line-height:43px;
     display:block;
    }

    .em_bodytext_grey ul {
   margin: 0px;
   padding: 0px;
   list-style-position: inside; 
   list-style-type:disc;
}
.em_bodytext_grey ul li {
   font: 14px #808080 Arial, sans-serif;
   line-height:20px;
   margin: 0;
   padding: 0;
}
.em_bodytext_grey ul li a {
    font: 14px #808080 Arial, sans-serif;
   text-decoration:none;
}       
.em_bg_blue {
    background-color: #437dc3 ;
        
}
.em_bg_pink {
    background-color:#43c3c3;
        
}
.em_bg_button {
    background-color:#d45656;
        
}
.em_bg_lightgrey {
    background-color:#e9e9e9;
        
}
@media only screen and (min-width:481px) and (max-width:649px) {
table[class=em_main_table] {
    width: 100% !important;
}
table[class=em_wrapper] {
    width: 100% !important;
}
table[class=em_tab_250] {
    width:250px !important;
}
td[class=em_hide], br[class=em_hide], table[class=em_hide] {
    display: none !important;
}
img[class=em_full_img] {
    width: 100% !important;
    height: auto !important;
    max-width:100% !important;
}

td[class=em_pad_top]{
    padding-top:20px !important;
}
td[class=em_pad_bottom]{
    padding-bottom:20px !important;
}
td[class=em_aside]{
    padding-left:20px !important;
    padding-right:20px !important;
}
td[class=em_height]{
    height: 20px !important;
}
td[class=em_space]{
    width:15px !important;  
}
td[class=em_headertext_grey] {
    text-align: center !important;
}
td[class=em_contact_text1_grey] {
    text-align: center !important;
}
td[class=em_contact_text2_grey] {
    text-align: center !important;
}
table[class=em_wrapper_50_1] {
    width: 50% !important;
    max-width:none !important;
    
}
}

@media only screen and (max-width:480px) {
table[class=em_main_table] {
    width: 100% !important;
}
table[class=em_wrapper] {
    width: 100% !important;
}
td[class=em_hide], br[class=em_hide], span[class=em_hide], table[class=em_hide] {
    display: none !important;
}
img[class=em_full_img] {
    width: 100% !important;
    height: auto !important;
    max-width:100% !important;
}

td[class=em_pad_top]{
    padding-top:20px !important;
}
td[class=em_pad_bottom]{
    padding-bottom:20px !important;
}
td[class=em_height]{
    height: 20px !important;
}
 td[class=em_aside]{
    padding-left:15px !important;
    padding-right:15px !important;
} 
td[class=em_space]{
    width:15px !important;  
}
td[class=em_white]{
    height: 20px !important;
    background-color:#ffffff !important;
}
td[class=em_headertext_grey] {
    text-align: center !important;
}
td[class=em_contact_text1_grey] {
    text-align: center !important;
}
td[class=em_contact_text2_grey] {
    text-align: center !important;
}
table[class=em_wrapper_50_1] {
    width: 50% !important;
    max-width:none !important;
    
}
}

</style>
</head>
<body style="margin:0px; padding:0px;" bgcolor="#ffffff">
<!-- === HEADER SECTION === -->
 <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
      <tr>
        
        <td align="center" valign="top">
          <table style="table-layout:fixed;" width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper">
            <tr>
              <td class="em_hide" height="1" style="line-height:0px; font-size:0px;"><img src="images/spacer.gif" height="1" width="650" style="display:block; width:650px; min-width:650px;" border="0" /></td>
            </tr>
           
            <tr>
              <td valign="top" align="center">
                <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper">
                  <tr>
                    <td valign="top">
                    <repeater>
                      <layout label='HEADER_1'>
                     <!-- === HEADER_1 === -->
                      <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper">
                        <tr>
                          <td  valign="top">
                            <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper">
                              <tr>
                                <td width="30" class="em_space">&nbsp;</td>
                                <td valign="top">
                                  <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
                                    <tr>
                                      <td height="20" class="em_height">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td valign="top">
                                      </td>
                                    </tr>
                                    <tr>
                                      <td height="25" class="em_height">&nbsp;</td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="30" class="em_space">&nbsp;</td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td class="em_bg_blue" valign="top">
                            <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper"  >
                              <tr>
                                <td height="36" class="em_height">&nbsp;</td>
                              </tr>
                              <tr>
                                <td align="center" valign="top">
                                  <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper">
                                    <tr>
                                      <td width="30" class="hide">&nbsp;</td>
                                      <td width="30" class="hide">&nbsp;</td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                              <tr>
                                <td height="36" class="em_height">&nbsp;</td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td  class="em_bg_pink" align="center"  valign="top">
                            <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper">
                              <tr>
                                <td height="14" style="line-height:1px; font-size:1px;">&nbsp;</td>
                              </tr>
                              <tr>
                                <td align="center" valign="top">
                              
                                </td>
                              </tr>
                              <tr>
                                <td height="14" style="line-height:1px; font-size:1px;">&nbsp;</td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                                      <td height="25" class="em_height">&nbsp;</td>
                                    </tr>
                      </table>
                      </layout>

                      </repeater>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <!-- Header //-->
          </table>
        </td>
      </tr>
    </table>
<!-- === //HEADER SECTION === -->
<!-- === BODY SECTION === -->
 <repeater>
     
            <layout label='TITLE + SUBTITLE_SECTION'>
         <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
          <tr>
           <td valign="top" align="center">
          <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper" style="table-layout:fixed;">
      <tr>
        <td width="45" class="em_space">&nbsp;</td>
        <td valign="top">
          <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">
            <tr>
              <td class="em_titletext_black" align="left"><singleline></singleline></td>
            </tr>
           
            <tr>
              <td height="14" style="line-height:1px;font-size:1px;">&nbsp;</td>
            </tr>
            <tr>
              <td  class="em_subtitletext_black" align="left"><multiline>
                Hello {{ $to }} <br><br>
                      The {{ $taskname }} Task you are working on Status has been updated .<br>
                      login to Follow Up. <br><br>

              <p>Kind Regards,</p>
              <p>Cytonn Pms Team.</p>
              </multiline></td>
            </tr>
            <tr>
              <td height="55" style="line-height:1px;font-size:1px;">&nbsp;</td>
            </tr>
          
          </table>
        </td>
        <td width="45" class="em_space">&nbsp;</td>
      </tr>
    </table>
         </td>
         </tr>
         </table>
         </layout>

            <layout label='SPACE_SECTION'>
           <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
          <tr>
           <td valign="top" align="center">
          <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper"  style="table-layout:fixed;">
      
         <tr>
                          <td height="20" class="em_height" style="line-height:0px;font-size:0px;"><img editable="true" src="spacer.gif" width="1" height="1" style="display:block;" border="0" alt=""  /></td>
            </tr>
    </table>
         </td>
         </tr>
         </table>
           </layout>
        <!-- === //SPACE_SECTION === -->
     </repeater>
  <!-- === BODY SECTION === -->
  <!-- === FOOTER SECTION === -->
 <table width="100%" border="0" align="center" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
      
                  <tr>
                    <td valign="top">
                    <repeater>
                   
                      <layout label='FOOTER_SECTION_3'>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       <tr>
                        <td valign="top" align="center">
                         <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper" style="table-layout:fixed;" >
                  <tr>
                    <td class="em_bg_blue">
                      <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper">
                        <tr>
                          <td height="35" class="em_height">&nbsp;</td>
                        </tr>
                        <tr>
                          <td align="center">
                            <table width="650" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper">
                              <tr>
                                <td width="100" class="em_space">&nbsp;</td>
                                <td>
                                  <table width="450" border="0" cellspacing="0" cellpadding="0" align="center" class="em_wrapper">

                                    <tr>
                                      <td height="20">&nbsp;</td>
                                    </tr>
                                     <tr>
                                      <td height="20">&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td align="center">
                                        <table width="92" border="0" cellspacing="0" cellpadding="0" align="center">
                                        </table>
                                      </td>
                                    </tr>
                                                                        <tr>
                                      <td  class="em_footertext_grey" align="center"><multiline>Copyright &copy;cytonn Pms<currentday> <currentmonth> <currentyear>, All rights reserved.
                                        </multiline>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                                <td width="100" class="em_space">&nbsp;</td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td height="35" class="em_height">&nbsp;</td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                                 </td>
                             </tr>
                            </table>
                         
                      </layout>
                      
   <!-- === //FOOTER_SECTION_3 === -->
                      </repeater>
                    </td>
                  </tr>
                  <tr>
                   <td style="font-size:1px; line-height:1px;"><unsubscribe>&nbsp;</unsubscribe></td>
                  </tr>
                </table>
             
<!-- === //FOOTER SECTION === -->

<div style="display:none; white-space:nowrap; font:20px courier; color:#ffffff; background-color:#ffffff;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></body>
</html>
