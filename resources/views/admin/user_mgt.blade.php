@extends('admin.header')
@section('content')

@include('admin.topbar')
    <div class="page-container">
        <div class="page-content">
            @include('admin.sidebar')
            <div class="content-wrapper">
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4> <span class="text-semibold"></span></h4>
                         </div>
                     </div>
                     <div class="breadcrumb-line breadcrumb-line-component bg-success">
                        <ul class="breadcrumb">
                            <li><a href="{{ URL::to('manager_dashboard')}}"><i class="icon-home2 position-left"></i> Dashboard </a></li>
                            <li class="active"> User Manager </li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                        </ul>
                    </div>
                </div>
               
               <div class="content">
                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="collapse"></a></li>
                                                <li><a data-action="reload"></a></li>
                                                <li><a data-action="close"></a></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="panel-body">
                                        <div class="row">                            
                                
                                <div class="col-lg-3">

                        <div class="table-responsive">
                            <table class="table table-xlg text-nowrap">
                                <div>
                                    <div class="media-left media-middle">
                                        <a href="{{url('add_user')}}" class="btn btn-rounded bg-success-400"></i>Add User</a>
                                    </div>
                                </div><br/>
                            </table>    
                        </div>              
                                   

                                </div>

                            </div>
                                <table class="table table-striped table-bordered table-hover" width="100%" id="staff">
                            <thead>
                                <tr>
                                    <th> No</th>
                                    <th> Designation</th>
                                    <th> First Name </th>
                                    <th> Middle Name </th>
                                    <th> Last Name </th>
                                    <th> UserType</th>
                                    <th> Mobilenumber </th>
                                    <th> Email </th> 
                                    <th> Department </th>
                                    <th> Job Level </th>
                                    <th> CreatedBy</th>                    
                                    <th> Status</th>
                                    <th> Created At</th>
                                    <th> Actions </th>
                                    
                                </tr>
                            </thead>
                             <tfoot>
                                <tr>
                                    <th> No</th>
                                    <th> Designation</th>
                                    <th> First Name </th>
                                    <th> Middle Name </th>
                                    <th> Last Name </th>
                                    <th> UserType</th>
                                    <th> Mobilenumber </th>
                                    <th> Email </th>
                                    <th> Department </th>
                                    <th> Job Level </th>
                                    <th> CreatedBy</th>                    
                                    <th> Status</th>
                                    <th> Created At</th>
                                    <th> Actions </th>
                                </tr>
                            </tfoot>
                        </table>
                                        
                        </div>
                    </div> 




                 @include('cytonnusers.footer')
                </div>
               
            </div>

        </div>
 <script type='text/javascript' charset="utf-8">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
            oTable = $('#staff').DataTable({
                    "contentType": 'application/jsonp; charset=utf-8',                
                    "processing": true,
                    "serverSide": true,            
                    "responsive": true,
                    "ordering": true,
                    "scrollX": true,
                    "paging": true,
                    "bSort": true,
                    "bFilter": true,
                    "lengthChange": true,
                    "ajax": "{{ URL::to('showallcytonnstaff') }}",
                    "columns": [
                        
                        {data: 'rownum', name: 'rownum','searchable':false},
                        {data: 'designation', name: 'designation.name'},
                        {data: 'firstname', name: 'staff.firstname'},
                        {data: 'middlename', name: 'staff.middlename'}, 
                        {data: 'lastname', name: 'staff.lastname'}, 
                        {data: 'usertype', name: 'usergroups.name'}, 
                        {data: 'mobilenumber', name: 'staff.mobilenumber'},
                        {data: 'email', name: 'staff.email'},
                        {data: 'department', name: 'departments.name'},
                        {data: 'joblevel', name: 'joblevel.name'},
                        {data: 'createdby_id', name: 'users.email'},
                        {data: 'status', name: 'status.name'},
                        {data: 'created_at', name: 'staff.created_at'},
                        {data: 'action', name: 'action','searchable':false}   
                    ]
                });
        });

    </script> 
    @stack('script')
 @endsection

