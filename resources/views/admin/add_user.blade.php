@extends('admin.header')
@section('content')

@include('admin.topbar')
    <div class="page-container">
        <div class="page-content">
            @include('admin.sidebar')
            <div class="content-wrapper">
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4> <span class="text-semibold"></span></h4>
                         </div>
                     </div>
                     <div class="breadcrumb-line breadcrumb-line-component bg-success">
                        <ul class="breadcrumb">
                            <li><a href="{{ URL::to('manager_dashboard')}}"><i class="icon-home2 position-left"></i> Dashboard </a></li>
                            <li><a href="{{ URL::to('user_mgt')}}"> User Manager </a></li>
                            <li class="active">Add User </li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                        </ul>
                    </div>
                </div>


    <!-- Content area -->
    <div class="content">                    
        <form class="form-horizontal" action="#" id="frmcreateuser">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>

                <div class="panel-body">
                    <div class="row">
                        <legend class="text-semibold"> User details</legend>
                         <div class="alert" id="createNotification" style="display: none;">
                    <ul id="ul" class="ul"></ul>
            </div>
                        <div class="col-md-6"> 

            <div class="form-group{{ $errors->has('designation') ? ' has-error' : '' }}">
                <label class="col-lg-3 control-label text-semibold">Designation:</label>
                <div class="col-lg-9">
                    <select name="designation" id="designation" class="form-control">
                        <option value="">Select Designation</option>
                      @foreach($getDesignation as $Designation)                        
                        <option value="{{$Designation->id}}">{{$Designation->name}}</option>
                      @endforeach  
                    </select>
                    @if($errors->has('designation'))
                    <span class="help-block">
                        <strong>{{ $errors->first('designation') }}</strong>
                    </span>
                    @endif
                </div> 
            </div>                        

                <div class="form-group{{$errors->has('firstname') ? ' has-error': '' }}">
                    <label class="col-lg-3 control-label text-semibold">First Name</label>
                    <div class="col-lg-9">
                        <input class="form-control" name="firstname" id="firstname" type="text">
                    </div>
                     @if($errors->has('firstname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('firstname') }}</strong>
                            </span>
                     @endif
                </div> 

                <div class="form-group{{$errors->has('middlename') ? ' has-error': '' }}">
                    <label class="col-lg-3 control-label text-semibold">Middle Name</label>
                    <div class="col-lg-9">
                        <input class="form-control" name="middlename" id="middlename" type="text">
                    </div>
                     @if($errors->has('middlename'))
                            <span class="help-block">
                                <strong>{{ $errors->first('middlename') }}</strong>
                            </span>
                     @endif
                </div>      

                <div class="form-group{{$errors->has('lastname') ? ' has-error': '' }}">
                    <label class="col-lg-3 control-label text-semibold">Last Name:</label>
                    <div class="col-lg-9">
                        <input class="form-control" name="lastname" id="lastname" type="text">
                    </div>
                     @if($errors->has('lastname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('lastname') }}</strong>
                            </span>
                     @endif
                </div>

                <div class="form-group{{ $errors->has('joblevel') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label text-semibold">Job Level:</label>
                    <div class="col-lg-9">                      
                         <select name="joblevel" id="joblevel" class="form-control" >
                             <option value=""> Select Job Level</option>
                            @foreach($getJoblevel as $usergroup)
                            <option  value="{{$usergroup->id}}">{{$usergroup->name}}</option>
                            @endforeach 
                        </select>
                        @if($errors->has('joblevel'))
                        <span class="help-block">
                            <strong>{{ $errors->first('joblevel') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>         
                                            
            </div>    

            <div class="col-md-6">

                <div class="form-group{{ $errors->has('usertype') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label text-semibold">UserType:</label>
                    <div class="col-lg-9">                      
                         <select name="usertype" id="usertype" class="form-control" >
                             <option value=""> Select UserType</option>
                            @foreach($getUserGroups as $usergroup)
                            <option  value="{{$usergroup->id}}">{{$usergroup->name}}</option>
                            @endforeach 
                        </select>
                        @if($errors->has('usertype'))
                        <span class="help-block">
                            <strong>{{ $errors->first('usertype') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>
                
                <div class="form-group{{$errors->has('email') ? ' has-error': '' }}">
                    <label class="col-lg-3 control-label text-semibold">Email:</label>
                    <div class="col-lg-9">
                        <input class="form-control" name="email" id="email" type="email">
                    </div>
                     @if($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                     @endif
                </div>   

                <div class="form-group{{$errors->has('mobilenumber') ? ' has-error': '' }}">
                    <label class="col-lg-3 control-label text-semibold">mobilenumber</label>
                    <div class="col-lg-9">
                        <input class="form-control" name="mobilenumber" id="mobilenumber" type="text">
                    </div>
                     @if($errors->has('mobilenumber'))
                            <span class="help-block">
                                <strong>{{ $errors->first('mobilenumber') }}</strong>
                            </span>
                     @endif
                </div>  

                <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label text-semibold">Department:</label>
                    <div class="col-lg-9">                      
                         <select name="department" id="department" class="form-control" >
                             <option value=""> Select Department</option>
                            @foreach($getDepartments as $usergroup)
                            <option  value="{{$usergroup->id}}">{{$usergroup->name}}</option>
                            @endforeach 
                        </select>
                        @if($errors->has('department'))
                        <span class="help-block">
                            <strong>{{ $errors->first('department') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>               

                <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label text-semibold">Status:</label>
                    <div class="col-lg-9">
                        <select name="status" id="status" class="form-control" >
                            <option value="">Select Status</option>
                            @foreach($getStatus as $status)
                            <option  value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach 
                        </select>
                        @if($errors->has('status'))
                        <span class="help-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>
 
                </div>
            </div>
                    <div class="text-right">
                        <button type="submit" id="createuser" class="btn btn-rounded btn-success">Add User</button>
                    </div>
                </div>
            </div>
        <div>                        
        </div>
    </form>
                     @include('cytonnusers.footer')
                </div>
               
            </div>

        </div>
         <script type='text/javascript' charset="utf-8">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

         //Javascript code to Assign Class
        $("#createuser").on('click', function (event) {

            var frm = $('#frmcreateuser');
            var btn = $('#createuser');            
            var designation = $('#designation').val();           
            var firstname = $('#firstname').val();
            var middlename = $('#middlename').val();
            var lastname = $('#lastname').val();
            var usertype = $('#usertype').val();
            var mobilenumber = $('#mobilenumber').val();
            var joblevel = $('#joblevel').val();
            var department = $('#department').val();
            var email = $('#email').val();
            var status = $('#status').val();
            var notification = $("#createNotification");
            notification.hide();
            var mydata = { 
                           "designation":designation,"firstname":firstname,
                           "middlename":middlename, "lastname":lastname,
                           "usertype":usertype, "mobilenumber":mobilenumber,
                           "joblevel":joblevel, "department":department,
                           "email":email,"status":status
                         }
            event.preventDefault();
            $.ajax({
                method: "POST",
                datatype: "json",
                contentType: "application/json",
                data: JSON.stringify(mydata),
                url: "createnewcytonnstaff",              
                success: function (data, status) {
                   // btn.hide();
                    notification.show();
                    notification.html('<div class="alert alert-success">' + '</code> Client Created Successfully' + '</div>');
                    notification.fadeOut(10000);
                    $("#frmcreateuser")[0].reset();

                },

                error: function (data) {
                    var errors = data.responseJSON;
                    console.log(errors);

                    $.each(errors, function (index, value) {
                        notification.show();
                        notification.html('<div class="alert alert-danger">' + '<li>' + value + '</li>' + '</div>');
                        notification.fadeOut(10000);
                        
                        });
                    }
                });
            });
            
        });

    </script> 
    @stack('script')

 @endsection

