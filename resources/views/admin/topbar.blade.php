	<!-- Main navbar -->
	<div class="navbar navbar-default header-highlight">
		<div class="navbar-header" style="background-color: white">
			<a class="navbar-brand" href="#"><img src="{{asset('assets/images/cytonn_logo.svg')}}" style="height: 35px;margin-top: -10px"></a>

			<ul class="nav navbar-nav visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">
			<ul class="nav navbar-nav" style="margin-left: -3px">
				<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>

			<div class="navbar-right">
				<p class="navbar-text"> <b>{{ Auth::user()->name }}</b> </p>
				<p class="navbar-text"><span class="label bg-success">ONLINE</span></p>
			</div>
		</div>
	</div>
	
