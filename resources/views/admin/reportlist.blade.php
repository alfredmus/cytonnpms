@extends('admin.header')
@section('content')

@include('admin.topbar')
<style type="text/css">    
    .modal-body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
}
</style>
    <div class="page-container">
        <div class="page-content">
            @include('admin.sidebar')
            <div class="content-wrapper">
                   
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4> <span class="text-semibold"></span></h4>
                         </div>
                     </div>
                     <div class="breadcrumb-line breadcrumb-line-component bg-success">
                        <ul class="breadcrumb">
                            <li><a href="{{ URL::to('manager_dashboard')}}"><i class="icon-home2 position-left"></i> Dashboard </a></li>
                            <li><a href="#"> Task &amp; Follow Up </a></li>
                            <li class="active"> Reports </li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="content">
                <div class="panel panel-flat">
                                    <div class="panel-heading">                                        
                                        <h5 class="panel-title">ALL REPORTS PER TASKS</h5>
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="collapse"></a></li>
                                                <li><a data-action="reload"></a></li>
                                                <li><a data-action="close"></a></li>
                                            </ul>
                                        </div>
                                    </div><hr>
                                       @include('flash_message')

                                    <div class="panel-body">
                                        <div class="row">                               

                                
                                <div class="col-lg-3">

                        <div>
                               
                        </div>
                    </div>
                </div>
                
        <table class="table table-striped table-bordered table-hover" width="100%" id="tasks">
            <thead>
                <tr>
                    <th> No </th>
                    <th> Task Name </th>
                    <th> Task Category </th>
                    <th> Priority </th>
                    <th> Department </th>
                    <th> Access Level </th> 
                    <th> Task Due Date </th>                    
                    <th> CreatedBy</th>
                    <th> Status </th>
                    <th> Date Created </th>
                    <th> Action </th>
                </tr>
            </thead>
             <tfoot>
                 <tr>
                    <th> No </th>
                    <th> Task Name </th>
                    <th> Task Category </th>
                    <th> Priority </th>
                    <th> Department </th>
                    <th> Access Level </th> 
                    <th> Task Due Date </th>                    
                    <th> CreatedBy</th>
                    <th> Status </th>
                    <th> Date Created </th>
                    <th> Action </th> 
                </tr>
            </tfoot>
    </table> 
                                        
                        </div>
                    </div> 
                       @include('cytonnusers.footer')                 
                </div>
            </div>
        </div>
<script type="text/javascript">
         $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-smRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        oTable = $('#tasks').DataTable({
            "processing": true,
            "serverSide": true,            
            "responsive": true,
            "ordering": true,
            "scrollX": true,
            "paging": true,
            "bSort": true,
            "bFilter": true,
            "lengthChange": true,
            "ajax": "{{ URL::to('ManagerShowReports') }}",
            "columns": [

                {data: 'rownum', name: 'rownum','searchable':false},
                {data: 'taskname', name: 'alltasks.taskname'},
                {data: 'taskcategory', name: 'alltasks.taskcategory'},
                {data: 'priority_id', name: 'priority.name'},
                {data: 'department_id', name: 'departments.name'},
                {data: 'accesslevel_id', name: 'accessleveltbl.name'},
                {data: 'taskduedate', name: 'alltasks.taskduedate'},
                {data: 'createdby_id', name: 'users.email'},
                {data: 'status_id', name: 'status.name'},
                {data: 'created_at', name: 'alltasks.created_at'},  
                {data: 'action', name: 'action','searchable':false}
                           
            ]
        });  

    }); 
</script>
@stack('script')
@endsection

