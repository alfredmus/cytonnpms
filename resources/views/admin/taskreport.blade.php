@extends('admin.header')
@section('content')

@include('admin.topbar')
<style type="text/css">    
    .fix1body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
}
</style>
    <div class="page-container">
        <div class="page-content">
            @include('admin.sidebar')
            <div class="content-wrapper">
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4> <span class="text-semibold"></span></h4>
                         </div>
                     </div>
                     <div class="breadcrumb-line breadcrumb-line-component bg-success">
                        <ul class="breadcrumb">
                            <li><a href="{{ URL::to('manager_dashboard')}}"><i class="icon-home2 position-left"></i> Dashboard </a></li>
                            <li><a href="{{ URL::to('managerReport')}}">All Tasks Reports </a></li>
                            <li class="active"> Task Report </li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                        </ul>
                    </div>
                </div>
               
               <div class="content">
                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">VIEW TASK REPORT</h5><br>
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="collapse"></a></li>
                                                <li><a data-action="reload"></a></li>
                                                <li><a data-action="close"></a></li>
                                            </ul>
                                        </div>
                                    </div><hr>

                                    <div class="panel-body">
                                        <div class="row">                            
                                
                                <div class="col-lg-3">
                                    <img src="{{asset('assets/images/cytonn_logo.svg')}}" style="height: 35px;margin-top: -10px; margin-left: 400px";>         
                                </div><br><br>
                    </div>
                    <div class="row">
                        @foreach($taskdetails as $taskd) 
                        <div class="col-lg-3">
                            <label class="text-semibold">TASK NAME</label>
                            <div class="alert alert-default alert-bordered">
                                    {{$taskd->taskname}}
                            </div>
                        </div>

                        <div class="col-lg-3">
                        <label class="text-semibold">TASK CATEGORY</label>
                        <div class="alert alert-default alert-bordered">
                                    {{$taskd->taskcategory}}
                        </div>
                        </div>

                        <div class="col-lg-3">
                        <label class="text-semibold">TASK DUEDATE</label>
                        <div class="alert alert-default alert-bordered">
                                    {{$taskd->taskduedate}}
                        </div>
                        </div>

                        <div class="col-lg-3">
                        <label class="text-semibold">TASK STATUS</label>
                        <div class="alert alert-default alert-bordered">
                                    {{$taskd->status_id}}
                        </div>
                        </div>
                        @endforeach
                    </div> 
                <hr>
                <label class="text-semibold">USERS ASSIGNED</label>
                    <div class="alert alert-default alert-bordered">
                        <table class="table table-striped table-bordered table-hover" width="100%" id="staff">
               
                            <thead>
                                <tr>
                                    <th> No</th>
                                    <th> Designation</th>
                                    <th> First Name </th>
                                    <th> Middle Name </th>
                                    <th> Last Name </th>
                                    <th> UserType</th>
                                    <th> Mobilenumber </th>
                                    <th> Email </th> 
                                    <th> Department </th>
                                    <th> Job Level </th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($userAssigned as $taskuser)   
                                <tr class="text-semibold">
                                    <td class="col-md-2">{{$taskuser->rownum}}</td>
                                    <td class="col-md-2">{{$taskuser->designation}}</td>
                                    <td class="col-md-2">{{$taskuser->firstname}}</td>
                                    <td class="col-md-2">{{$taskuser->middlename}}</td>
                                    <td class="col-md-2">{{$taskuser->lastname}}</td>
                                    <td class="col-md-2">{{$taskuser->usertype}}</td>
                                    <td class="col-md-2">{{$taskuser->mobilenumber}}</td>
                                    <td class="col-md-2">{{$taskuser->email}}</td>
                                    <td class="col-md-2">{{$taskuser->department}}</td>
                                    <td class="col-md-2">{{$taskuser->joblevel}}</td>
                                </tr> 
                                @endforeach 
                            </tbody>
                             <tfoot>
                                <tr>
                                    <th> No</th>
                                    <th> Designation</th>
                                    <th> First Name </th>
                                    <th> Middle Name </th>
                                    <th> Last Name </th>
                                    <th> UserType</th>
                                    <th> Mobilenumber </th>
                                    <th> Email </th>
                                    <th> Department </th>
                                    <th> Job Level </th>
                                </tr>
                            </tfoot>
                        </table>
                            </div>
                            <hr>
                <label class="text-semibold">TASK DOCUMENT</label>
                    <div class="alert alert-default alert-bordered">
                        <div class="fix1body">
                        @foreach($taskdetails as $taskd) 
                          <img src="/cytonnpms/public/{{$taskd->taskdocument}}">
                        @endforeach  
                        </div>
                    </div>            
                        </div>
                    </div> 

                 @include('cytonnusers.footer')
                </div>
               
            </div>

        </div>
 <script type='text/javascript' charset="utf-8">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
        //Assigned users table properties    
            oTable = $('#staff').DataTable({           
                    "responsive": true,
                    "ordering": true,
                    "scrollX": true,
                    "paging": true,
                    "bSort": true,
                    "bFilter": true,
                    "lengthChange": true
                });
             });

    </script> 
    @stack('script')
 @endsection

