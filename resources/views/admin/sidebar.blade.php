<div class="sidebar sidebar-main sidebar-default">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user-material">
						<div class="category-content">
							<div class="sidebar-user-material-content">
								<a href="#"><img src="{{asset('assets/images/placeholder.png')}}" class="img-circle img-responsive" alt=""></a>
							</div>
														
							<div class="sidebar-user-material-menu" style="background-color: darkgreen">
								<a href="#user-nav" data-toggle="collapse"><span>My account</span> <i class="caret"></i></a>
							</div>
						</div>
						
						<div class="navigation-wrapper collapse" id="user-nav">
							<ul class="navigation">
								<li><a href="{{url('managerprofile')}}"><i class="icon-user-plus"></i> <span>My profile</span></a></li>
								<li class="divider"></li>
								<li><a href="{{url('adminsettings')}}"><i class="icon-cog5"></i> <span>Account settings</span></a></li>
								<li>
									<a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="icon-switch2"></i> 
                                            <span>Logout</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
								</li>
							</ul>
						</div>
					</div>


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header animated slideInLeft"><span>Main</span> <i class="icon-menu" title="" data-original-title="Main pages"></i></li>
								<li class="active"><a href="{{url('manager_dashboard')}}"><i class="icon-home4"></i> <span> Dashboard</span></a></li>

								<li><a href="{{url('comingsoon')}}"><i class="icon-stack2"></i> <span>Projects</span></a></li>

								<li><a href="{{url('comingsoon')}}"><i class="glyphicon glyphicon-indent-left"></i> <span>My Projects</span></a></li>

								<li><a href="{{url('comingsoon')}}"><i class="icon-bubbles2"></i> <span>Issues</span></a></li>

								<li><a href="{{url('comingsoon')}}"><i class="icon-book"></i> <span>User Board</span></a></li>

								<li><a href="#"><i class="icon-select2"></i> <span>Task &amp; Follow Up</span></a>
								<ul>
									<li><a href="{{url('manageruserboard')}}">User Board</a></li>
									<li><a href="{{url('findTaskPerUser')}}">My Tasks</a></li>
									<li><a href="{{url('managerReport')}}">Reports</a></li>
								</ul>
								</li>

								<li><a href="{{url('user_mgt')}}"><i class="icon-user"></i> <span>User Manager</span></a></li>
						
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>