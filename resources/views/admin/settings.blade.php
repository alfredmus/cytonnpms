@extends('admin.header')
@section('content')
@include('admin.topbar')
    <div class="page-container">
        <div class="page-content">
            @include('admin.sidebar')
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4> <span class="text-semibold"></h4>
                        </div>
                                
                </div>
<!-- Content area -->
<div class="content">

	<!-- Detached content -->
	<div class="container-detached">
		<div class="content-detached">							
		<!-- Account settings -->
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h6 class="panel-title">CHANGE ACCOUNT CREDENTIALS</h6>				
				<div class="heading-elements">
					<ul class="icons-list">
                		<li><a data-action="collapse"></a></li>
                		<li><a data-action="reload"></a></li>
                		<li><a data-action="close"></a></li>
                	</ul>
            	</div>
			</div><hr>
				<div class="alert" id="createNotification" style="display: none;">
                    <ul id="ul" class="ul"></ul>
            	</div>
	<div class="panel-body">		
		<form action="#" id="frmsettings">
			<div class="form-group">
				<div class="row">					
				<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label"><b>Email</b></label>
                    <div class="col-lg-6">
                        <input class="form-control" type="email" name="email" id="email" >
                        </select>
                        @if($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>
			 </div>
			</div>

			<div class="form-group">
				<div class="row">
				<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label"><b>Password</b></label>
                    <div class="col-lg-6">
                        <input class="form-control" type="password" name="password" id="password" >
                        </select>
                        @if($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">
				<div class="form-group{{ $errors->has('confirmpassword') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label"><b>Confirm Password</b></label>
                    <div class="col-lg-6">
                        <input class="form-control" type="password" name="confirmpassword" id="confirmpassword" >
                        </select>
                        @if($errors->has('confirmpassword'))
                        <span class="help-block">
                            <strong>{{ $errors->first('confirmpassword') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>
				</div>
			</div>
					

                    <div class="text-right">
                    	<button id="btnsettings" class="btn btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
			</div>
		</div>
	</div>
	</div> @include('cytonnusers.footer')
				</div>
			</div>
				
		</div>
	</div>
</div>
 <script>
  	$(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-smRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $("#btnsettings").on('click', function (event) {
            var frm = $('#frmsettings');
            var btn = $('#btnsettings');
            var email = $('#email').val();
            var password = $('#password').val();
            var confirmpassword = $('#confirmpassword').val();
            var notification = $("#createNotification");
            notification.hide();
            var mydata = { "email":email, "password":password,
            			   "confirmpassword":confirmpassword
            			 }

            event.preventDefault();
            $.ajax({
                method: "POST",
                datatype: "json",
                contentType: "application/json",
                data: JSON.stringify(mydata),
                url: "updateusercredentials",              
                success: function (data, status) {
                    btn.hide();
                    notification.show();
                    notification.html('<div class="alert alert-success">' + '<code>'  +  '</code> Updated Successfully' + '</div>');
                    notification.fadeOut(10000);

                },

                error: function (data) {
                    var errors = data.responseJSON;
                    console.log(errors);

                    $.each(errors, function (index, value) {
                        notification.show();
                        notification.html('<div class="alert alert-danger">' + '<li>' + value + '</li>' + '</div>');
                        notification.fadeOut(10000);                        
                        });
                    }
                });
            });
    });
  </script>

@endsection
