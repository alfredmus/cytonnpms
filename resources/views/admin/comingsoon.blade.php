@extends('admin.header')
@section('content')

	<!-- Page container -->
	<div style="background-color: white;">
	<div class="page-container">
		<div class="page-content">
			<div class="content-wrapper">
				<div class="content">

					<div class="text-center content-group">
						<h1  style="color: green; font-size: 100px">Cytonn Task &amp; Follow Up</h1>
						<h2><i style="color: red ">We are sorry for the inconvenience, Coming Soon!</i></h2>
					</div>
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
				<div class="text-center">			
					<a class="btn bg-danger-400"
                            onclick="window.history.go(-1); return false;">
                            <span>Go to Back</span>
                            <i class="icon-arrow-right14 position-left"></i>                            
                    </a>
				</div>				
						</div>
					</div>
					 @include('cytonnusers.footer')  

				</div>
			</div>
		</div>
	</div>
	</div>
  @endsection
