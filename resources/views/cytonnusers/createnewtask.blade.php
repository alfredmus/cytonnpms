
@extends('admin.header')
@section('content')

@include('admin.topbar')
<!-- Start Create New Task Blade -->
<div class="page-container">
    <div class="page-content">
        @include('cytonnusers.sidebar')
        <div class="content-wrapper">

             <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4> <span class="text-semibold"></span></h4>
                         </div>
                     </div>
                     <div class="breadcrumb-line breadcrumb-line-component bg-success">
                        <ul class="breadcrumb">
                            <li><a href="{{ URL::to('admin_dashboard')}}"><i class="icon-home2 position-left"></i> Dashboard </a></li>
                            <li><a href="#"> Task &amp; Follow Up </a></li>
                            <li> <a href="{{url('mytasks')}}">
                             User Board </a></li>
                            <li class="active"> Create Task </li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                        </ul>
                    </div>
                </div>

                     @include('flash_message')
                
	  <div class="content"> 
  	<form  action="{{ URL::to('addnewtask') }}" class="form-horizontal" method="post" enctype="multipart/form-data">

	<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">CREATE NEW TASK<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<li><a data-action="reload"></a></li>
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div><hr>

		<div class="panel-body">
			<div class="row">
			<div class="col-md-6">

				<div class="form-group{{$errors->has('taskname') ? ' has-error': '' }}">
					<label class="col-lg-3 control-label">Task name</label>
					<div class="col-lg-9">
						<input class="form-control" name="taskname" id="taskname" type="text">
					</div>
					 @if($errors->has('taskname'))
                            <span class="help-block">
                                <strong>{{ $errors->first('taskname') }}</strong>
                            </span>
                     @endif
				</div>

				<div class="form-group{{$errors->has('taskcategory') ? ' has-error': '' }}">
					<label class="col-lg-3 control-label">Task Category</label>
					<div class="col-lg-9">
						<input class="form-control" name="taskcategory" id="taskcategory" type="text">
					</div>
					 @if($errors->has('taskcategory'))
                            <span class="help-block">
                                <strong>{{ $errors->first('taskcategory') }}</strong>
                            </span>
                     @endif
				</div>	

				<div class="form-group{{ $errors->has('priority') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label text-semibold">Priority</label>
                    <div class="col-lg-9">
                        <select name="priority" id="priority" class="form-control" >
                        	<option  value=""> Please Select Priority </option>
                            @foreach($getPriority as $stat)
                            <option  value="{{$stat->id}}">{{$stat->name}} </option>
                            @endforeach 
                        </select>
                        @if($errors->has('priority'))
                        <span class="help-block">
                            <strong>{{ $errors->first('priority') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>

				<div class="form-group{{ $errors->has('departments') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label text-semibold">Department</label>
                    <div class="col-lg-9">
                        <select name="departments" id="departments" class="form-control" >
                        	<option  value=""> Please Select Department </option>
                            @foreach($getDepartments as $stat)
                            <option  value="{{$stat->id}}">{{$stat->name}} </option>
                            @endforeach 
                        </select>
                        @if($errors->has('departments'))
                        <span class="help-block">
                            <strong>{{ $errors->first('departments') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>

				<div class="form-group{{ $errors->has('accesslevel') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label text-semibold">Access Level</label>
                    <div class="col-lg-9">
                        <select name="accesslevel" id="accesslevel" class="form-control" >
                        	<option  value=""> Please Select Access Level </option>
                            @foreach($getAccessLevel as $stat)
                            <option  value="{{$stat->id}}">{{$stat->name}} </option>
                            @endforeach 
                        </select>
                        @if($errors->has('accesslevel'))
                        <span class="help-block">
                            <strong>{{ $errors->first('accesslevel') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>
			</div>

			<div class="col-md-6">
				<div class="form-group{{$errors->has('duedate') ? ' has-error': '' }}">
                    <label class="col-lg-3 control-label text-semibold">Due Date</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="icon-calendar3"></i></span>
                         <input type="text" class="form-control pickadate-accessibility picker__input picker__input--active" readonly="" id="P284084789" name="duedate" aria-haspopup="true" aria-expanded="true" aria-readonly="false" aria-owns="P284084789_root">
                    </div>
                     @if($errors->has('duedate'))
                            <span class="help-block">
                                <strong>{{ $errors->first('duedate') }}</strong>
                            </span>
                     @endif
                </div> 	

				<div class="form-group{{$errors->has('taskdocument') ? ' has-error': '' }}">
					<label class="col-lg-3 control-label text-semibold"> Task Document </label>
					<div class="col-lg-9">
						<input type="file" name="taskdocument" id="taskdocument" class="file-input" data-show-upload="false" data-show-remove="false" data-show-preview="false">
						<span class="help-block"> Upload only <code>Jpeg</code>| <code>Png</code>  file extensions.</span>
					</div>
					@if($errors->has('taskdocument'))
                            <span class="help-block">
                                <strong>{{ $errors->first('taskdocument') }}</strong>
                            </span>
                     @endif
				</div>	

				<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                    <label class="col-lg-3 control-label text-semibold">Status</label>
                    <div class="col-lg-9">
                        <select name="status" id="status" class="form-control" >
                            @foreach($getStatus as $stat)
                            <option  value="{{$stat->id}}">{{$stat->name}} </option>
                            @endforeach 
                        </select>
                        @if($errors->has('status'))
                        <span class="help-block">
                            <strong>{{ $errors->first('status') }}</strong>
                        </span>
                        @endif
                    </div> 
                </div>

			</div>
			</div>

		<div class="text-right">
			<button type="submit" class="btn btn-rounded btn-success">CREATE TASK</button>
		</div>	
	</form><br><br><br>		
	</div>	@include('cytonnusers.footer')
</div>
</div>	
</div>
<!-- End Create New Task Blade -->
@endsection

