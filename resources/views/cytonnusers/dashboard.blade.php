@extends('admin.header')
@section('content')

@include('admin.topbar')
<script type="text/javascript" src="{{asset('assets/js/pages/dashboard.js')}}"></script>
<script src="{{asset('assets/amcharts/amcharts.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/amcharts/pie.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/amcharts/serial.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/amcharts/plugins/export/export.css')}}" type="text/css" media="all"></script>
<script src="{{asset('assets/amcharts/themes/light.js')}}" type="text/javascript"></script>

<div class="page-container">
        <div class="page-content">
            @include('cytonnusers.sidebar')
            <div class="content-wrapper">

            <div class="content">               
                    <div class="row"> 
                     <div class="col-lg-6"> 
                    <div class="panel panel-flat"> 
                    <div class="panel-heading"> 
                    <h5 class="panel-title">Public vs Private</h5> 
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div> 
                    </div>                      
                        <div id="chartdiv" style="width: 100%; height: 500px;"></div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="panel panel-flat"> 
                    <div class="panel-heading">
                    <h5 class="panel-title">Closed vs Ongoing</h5>   
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div> 
                    </div>                      
                        <div id="chartdiv1" style="width: 100%; height: 500px;"></div>
                      </div>
                  </div>
                    </div>          @include('cytonnusers.footer')              
            </div>        
        </div>
    </div>                                          
</div>
<!-- Chart code -->
<script type='text/javascript' charset="utf-8">
      
      //Fetch dashboard data
                var Closed = <?php echo $closed; ?>;
                var Ongoing = <?php echo $ongoing; ?>;
                var Public = <?php echo $public; ?>;
                var Private = <?php echo $private; ?>;
                   
               var chart1 = AmCharts.makeChart( "chartdiv1", {
                  "type": "pie",
                  "theme": "none",
                  "dataProvider": [ {
                    "title": "Ongoing",
                    "color": "green",
                    "value": Ongoing
                  }, {
                    "title": "Closed",
                    "color": "black",
                    "value": Closed
                  } ],
                  "titleField": "title",
                  "valueField": "value",
                  "labelRadius": 5,

                  "radius": "42%",
                  "innerRadius": "60%",
                  "labelText": "[[title]]",
                  "export": {
                    "enabled": true
                  }
                });

                var chart = AmCharts.makeChart("chartdiv", {
                  "type": "serial",
                  "theme": "light",
                  "marginRight": 70,
                  "dataProvider": [{
                    "country": "Public",
                    "visits": Public,
                    "color": "green"
                  }, {
                    "country": "Private",
                    "visits": Private,
                    "color": "black"
                  }],
                  "valueAxes": [{
                    "axisAlpha": 0,
                    "position": "left",
                    "title": "Public Task vs Private Tasks"
                  }],
                  "startDuration": 1,
                  "graphs": [{
                    "balloonText": "<b>[[category]]: [[value]]</b>",
                    "fillColorsField": "color",
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "visits"
                  }],
                  "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                  },
                  "categoryField": "country",
                  "categoryAxis": {
                    "gridPosition": "start",
                    "labelRotation": 45
                  },
                  "export": {
                    "enabled": true
                  }

                }); 
  </script>


@endsection