@extends('admin.header')
@section('content')

@include('admin.topbar')
<style type="text/css">    
    .modal-body {
        max-height: calc(100vh - 210px);
        overflow-y: auto;
}
</style>
    <div class="page-container">
        <div class="page-content">
            @include('cytonnusers.sidebar')
            <div class="content-wrapper">
                   
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4> <span class="text-semibold"></span></h4>
                         </div>
                     </div>
                     <div class="breadcrumb-line breadcrumb-line-component bg-success">
                        <ul class="breadcrumb">
                            <li><a href="{{ URL::to('admin_dashboard')}}"><i class="icon-home2 position-left"></i> Dashboard </a></li>
                            <li><a href="#"> Task &amp; Follow Up </a></li>
                            <li class="active"> User Board </li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="content">
                <div class="panel panel-flat">
                    <div class="panel-heading">                                        
                        <h5 class="panel-title">ALL PUBLIC TASKS</h5>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div><hr>
                       @include('flash_message')
                    <div class="panel-body">
                        <div class="row">
                    <div class="col-lg-3">
                    <div>
                            <div class="media-left media-middle">
                                <a href="{{url('newtask')}}" class="btn btn-rounded bg-success-400"></i> Create  Task</a>
                            </div>
                        </div>
                    </div>
                </div>
                
        <table class="table table-striped table-bordered table-hover" width="100%" id="tasks">
            <thead>
                <tr>
                    <th> No </th>
                    <th> Task Name </th>
                    <th> Task Category </th>
                    <th> Priority </th>
                    <th> Department </th>
                    <th> Access Level </th> 
                    <th> Task Document </th>
                    <th> Task Due Date </th>                    
                    <th> CreatedBy</th>
                    <th> Status </th>
                    <th> Date Created </th>
                    <th> Assign Users</th>
                    <th> Action </th>
                </tr>
            </thead>
            <tbody>
                @foreach($privatetasks as $taskassg)   
                <tr class="text-semibold">
                    <td class="col-md-2">{{$taskassg->rownum}}</td>
                    <td class="col-md-2">{{$taskassg->taskname}}</td>
                    <td class="col-md-2">{{$taskassg->taskcategory}}</td>
                    <td class="col-md-2">{{$taskassg->priority_id}}</td>
                    <td class="col-md-2">{{$taskassg->department_id}}</td>
                    <td class="col-md-2">{{$taskassg->accesslevel_id}}</td>
                    <td class="col-md-2">
                        <a href="#" class="btn btn-rounded btn-default" data-toggle="modal" data-target="#{{$taskassg->id}}"></i>View {{$taskassg->taskname}} Document</a>
                        <!--Start taskdocument View -->
                        <div id="{{$taskassg->id}}" class="modal fade">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header bg-success">
                                        <button type="button" class="close" data-dismiss="modal">&times;
                                        </button>
                                        <h6 class="modal-title">VIEW TASK DOCUMENT</h6>
                                    </div>

                                    <div class="modal-body">
                                        <img src="/cytonnpms/public/{{$taskassg->taskdocument}}">
                                    </div>

                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End taskdocument View -->

                    </td>
                    <td class="col-md-2">{{$taskassg->taskduedate}}</td>
                    <td class="col-md-2">{{$taskassg->createdby_id}}</td>
                    <td class="col-md-2">{{$taskassg->status_id}}</td>
                    <td class="col-md-2">{{$taskassg->created_at}}</td>
                    <td class="col-md-2">
                        <div class="btn-group">
                            <a href="showtaskuser/{{$taskassg->id}}" type="button" class="btn btn-rounded btn-default"> ASSIGN TASK USER </a>
                        </div>
                    </td>
                    <td class="col-md-2">
                        <div class="btn-group">                            
                            <button type="button" class="btn btn-rounded btn-success openModal"  data-id="{{$taskassg->id}}" data-toggle="modal" data-target="#edittask">
                            EDIT </button>
                        </div>
                    </td>
                </tr> 
                @endforeach 
            </tbody>
             <tfoot>
                 <tr>
                    <th> No </th>
                    <th> Task Name </th>
                    <th> Task Category </th>
                    <th> Priority </th>
                    <th> Department </th>
                    <th> Access Level </th> 
                    <th> Task Document </th>
                    <th> Task Due Date </th>                    
                    <th> CreatedBy</th>
                    <th> Status </th>
                    <th> Date Created </th>
                    <th> Assign Users</th>
                    <th> Action </th> 
                </tr>
            </tfoot>
    </table> 

<!--Start edittask -->
<div id="edittask" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">EDIT TASK INFORMATION</h6>
            </div>            

            <div class="modal-body">
                <div class="alert" id="editNotification" style="display: none;">
                <ul id="ul" class="ul"></ul>
                </div>
                <form action="#" id="frmEditTask">
                <div class="row">
                    <div class="col-md-6">
                        <label class="col-lg-3 control-label text-semibold">Task Name</label>
                        <div class="col-lg-9">
                            <input class="form-control text-semibold" name="edittaskname" id="edittaskname" type="text" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-lg-3 control-label text-semibold">Task Category</label>
                        <div class="col-lg-9">
                            <input class="form-control text-semibold" name="edittaskcategory" id="edittaskcategory" type="text" readonly>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label class="col-lg-3 control-label text-semibold">AccessLevel</label>
                        <div class="col-lg-9">
                        <select name="editaccesslevel" id="editaccesslevel" class="form-control" >
                           @foreach($getAccessLevel as $status)
                            <option  value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="col-lg-3 control-label text-semibold">Status</label>
                        <div class="col-lg-9">
                        <select name="editstatus" id="editstatus" class="form-control" >
                           @foreach($getStatus as $status)
                            <option  value="{{$status->id}}">{{$status->name}}</option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                </form>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
                <button id="btnEditTask" class="btn btn-primary btn border-slate text-slate-800 btn-flat  text-right">EDIT</button>
            </div>
        </div>
    </div>
</div>
<!--End edittask -->                            
                                        
                        </div>
                    </div> 
                       @include('cytonnusers.footer')                 
                </div>
            </div>
        </div>
<script type="text/javascript">
         $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-smRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

        oTable = $('#tasks').DataTable({         
            "responsive": true,
            "ordering": true,
            "scrollX": true,
            "paging": true,
            "bSort": true,
            "bFilter": true,
            "lengthChange": true
        });  

 // Start fetch task Edit
    $(document).on("click", ".openModal", function (event) {

    var taskid = $(this).data('id');
    $.get('{{ url("findTaskById") }}/' + taskid + "", function(data) {
    $.each(data, function(index,subCatObj){ 

        document.getElementById('edittaskname').value = subCatObj.taskname;
        document.getElementById('edittaskcategory').value = subCatObj.taskcategory;
        $('#editaccesslevel').append("<option value='"
                                                     + subCatObj.id +"'>"
                                                     + subCatObj.accesslevel_id + 
                                                     "</option>"
                                                 );
        $('#editstatus').append("<option value='"
                                                     + subCatObj.id +"'>"
                                                     + subCatObj.status_id + 
                                                     "</option>"
                                                 );

            }); 
        });
    }); 
//  End fetch task  to edit

//Start Edit task
 $("#btnEditTask").on('click', function (event) {
            var frm = $('#frmEditTask');
            var btn = $('#btnEditTask');
            var editaccesslevel = $('#editaccesslevel').val();
            var edittaskname = $('#edittaskname').val();
            var editstatus = $('#editstatus').val();
            var notification = $("#editNotification");
            notification.hide();
            var mydata = { "editaccesslevel":editaccesslevel,"edittaskname":edittaskname,
                           "editstatus":editstatus }

            event.preventDefault();
            $.ajax({
                method: "POST",
                datatype: "json",
                contentType: "application/json",
                data: JSON.stringify(mydata),
                url: "updateTasks",              
                success: function (data, status) {
                    btn.hide();
                    notification.show();
                    notification.html('<div class="alert alert-success">' + '<code>'  +  '</code> Updated Successfully' + '</div>');
                    notification.fadeOut(10000);

                },

                error: function (data) {
                    var errors = data.responseJSON;
                    console.log(errors);

                    $.each(errors, function (index, value) {
                        notification.show();
                        notification.html('<div class="alert alert-danger">' + '<li>' + value + '</li>' + '</div>');
                        notification.fadeOut(10000);                        
                        });
                    }
                });
            });


//End Edit task
    }); 
</script>
@stack('script')
@endsection

