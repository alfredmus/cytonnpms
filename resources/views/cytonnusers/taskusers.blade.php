@extends('admin.header')
@section('content')

@include('admin.topbar')
    <div class="page-container">
        <div class="page-content">
            @include('cytonnusers.sidebar')
            <div class="content-wrapper">
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4> <span class="text-semibold"></span></h4>
                         </div>
                     </div>
                     <div class="breadcrumb-line breadcrumb-line-component bg-success">
                        <ul class="breadcrumb">
                            <li><a href="{{ URL::to('admin_dashboard')}}"><i class="icon-home2 position-left"></i> Dashboard </a></li>
                            <li><a href="{{ URL::to('mytasks')}}">My Tasks </a></li>
                            <li class="active"> User Assigned Task </li>
                        </ul>

                        <ul class="breadcrumb-elements">
                            <li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
                        </ul>
                    </div>
                </div>
               
               <div class="content">
                <div class="panel panel-flat">
                                    <div class="panel-heading">
                                        <h5 class="panel-title">VIEW ASSIGNED USERS </h5>
                                        <div class="heading-elements">
                                            <ul class="icons-list">
                                                <li><a data-action="collapse"></a></li>
                                                <li><a data-action="reload"></a></li>
                                                <li><a data-action="close"></a></li>
                                            </ul>
                                        </div>
                                    </div><hr>

                                    <div class="panel-body">
                                        <div class="row">                            
                                
                                <div class="col-lg-3">

                        <div>
                                <div>
                                    <div class="media-left media-middle">
                                        <a href="#" class="btn btn-rounded bg-success-400" data-toggle="modal" data-target="#editclient"></i>Assign New User</a>
                                    </div>
                                </div>   
                        </div>              
                             <input type="text" id="usertaskid" name="usertaskid" value="{{$taskid}}" style="display: none;">
                        </div>
                    </div>
                        <table class="table table-striped table-bordered table-hover" width="100%" id="staff">
               
                            <thead>
                                <tr>
                                    <th> No</th>
                                    <th> Designation</th>
                                    <th> First Name </th>
                                    <th> Middle Name </th>
                                    <th> Last Name </th>
                                    <th> UserType</th>
                                    <th> Mobilenumber </th>
                                    <th> Email </th> 
                                    <th> Department </th>
                                    <th> Job Level </th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($userAssigned as $taskuser)   
                                <tr class="text-semibold">
                                    <td class="col-md-2">{{$taskuser->rownum}}</td>
                                    <td class="col-md-2">{{$taskuser->designation}}</td>
                                    <td class="col-md-2">{{$taskuser->firstname}}</td>
                                    <td class="col-md-2">{{$taskuser->middlename}}</td>
                                    <td class="col-md-2">{{$taskuser->lastname}}</td>
                                    <td class="col-md-2">{{$taskuser->usertype}}</td>
                                    <td class="col-md-2">{{$taskuser->department}}</td>
                                    <td class="col-md-2">{{$taskuser->joblevel}}</td>
                                    <td class="col-md-2">{{$taskuser->mobilenumber}}</td>
                                    <td class="col-md-2">{{$taskuser->email}}</td>
                                </tr> 
                                @endforeach 
                            </tbody>
                             <tfoot>
                                <tr>
                                    <th> No</th>
                                    <th> Designation</th>
                                    <th> First Name </th>
                                    <th> Middle Name </th>
                                    <th> Last Name </th>
                                    <th> UserType</th>
                                    <th> Mobilenumber </th>
                                    <th> Email </th>
                                    <th> Department </th>
                                    <th> Job Level </th>
                                </tr>
                            </tfoot>
                        </table>
                                        
                        </div>
                    </div> 

<!--Start editclient -->
<div id="editclient" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h6 class="modal-title">SELECT USER</h6>
            </div>

            <div class="modal-body">
                    <table class="table table-striped table-bordered table-hover" width="100%" id="assignstaff">
                            <thead>
                                <tr>
                                    <th> No</th>
                                    <th> Designation</th>
                                    <th> First Name </th>
                                    <th> Middle Name </th>
                                    <th> Last Name </th>
                                    <th> Department </th>
                                    <th> Job Level </th>
                                    <th> Action </th>
                                    
                                </tr>
                            </thead>
                        </table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-link" data-dismiss="modal">Close</button>            </div>
        </div>
    </div>
</div>
<!--End editclient -->

                 @include('cytonnusers.footer')
                </div>
               
            </div>

        </div>
 <script type='text/javascript' charset="utf-8">
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-XSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            
        //Assigned users table properties    
            oTable = $('#staff').DataTable({           
                    "responsive": true,
                    "ordering": true,
                    "scrollX": true,
                    "paging": true,
                    "bSort": true,
                    "bFilter": true,
                    "lengthChange": true,
                });

            //Show all staff 
            oTable1 = $('#assignstaff').DataTable({
                    "contentType": 'application/jsonp; charset=utf-8',                
                    "processing": true,
                    "serverSide": true,            
                    "responsive": true,
                    "ordering": true,
                    "scrollX": true,
                    "paging": true,
                    "bSort": true,
                    "bFilter": true,
                    "lengthChange": true,
                    "ajax":'{{ url("selectCytonnStaff") }}',
                    "columns": [
                        
                        {data: 'rownum', name: 'rownum','searchable':false},
                        {data: 'designation', name: 'designation.name'},
                        {data: 'firstname', name: 'staff.firstname'},
                        {data: 'middlename', name: 'staff.middlename'}, 
                        {data: 'lastname', name: 'staff.lastname'}, 
                        {data: 'department', name: 'departments.name'},
                        {data: 'joblevel', name: 'joblevel.name'},
                        {data: 'action', name: 'action','searchable':false},   
                    ]
                });

            //Assign User Task
            $(document).on("click", ".selectTaskStaff", function () {
                var userID = $(this).data('id');
                var taskid = $('#usertaskid').val();
                var dataString = {staff_id: userID, alltasks_id: taskid};
                $.ajax({
                    type: "POST",
                    url: "<?php echo url('assigntaskstaff'); ?>",
                    data: dataString,
                    cache: false,
                    success: function(data, status){
                        alert('Assigned Successfully');
                    }
                });
            });

        });

    </script> 
    @stack('script')
 @endsection

