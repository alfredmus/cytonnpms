@extends('admin.header')
@section('content')
@include('admin.topbar')
    <div class="page-container">
        <div class="page-content">
            @include('cytonnusers.sidebar')
            <div class="content-wrapper">

                <!-- Page header -->
                <div class="page-header">
                    <div class="page-header-content">
                        <div class="page-title">
                            <h4> <span class="text-semibold"></h4>
                        </div>
                                
                </div>
				<div class="content">		
					<div class="container-detached">
						<div class="content-detached">

							<!-- Tab content -->
							<div class="tab-content">
								<div class="tab-pane fade in active" id="profile">


									<!-- Profile info -->
									<div class="panel panel-flat">
										<div class="panel-heading">
											<h6 class="panel-title">PROFILE INFORMATION</h6>
											<div class="heading-elements">
												<ul class="icons-list">
							                		<li><a data-action="collapse"></a></li>
							                		<li><a data-action="reload"></a></li>
							                		<li><a data-action="close"></a></li>
							                	</ul>
						                	</div>
										</div>

										<div class="panel-body">
											<form action="#">
									<div class="row">				
									<div class="col-md-4 bg-primary-400 border-radius-top text-center">										

										<a href="#" class="display-inline-block content-group-sm">
											<img src="{{ $getStaff->fullname }}" class="img-circle img-responsive" alt="" style="width: 110px; height: 210px;">
										</a>
									</div>
								</div><br/><br/>
			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<label><b>FullName</b></label>
						<input type="text" value="{{ $getStaff->fullname }}" class="form-control" readonly>
					</div>
					<div class="col-md-6">
						<label><b>Phone</b></label>
						<input type="text" value="{{ $getStaff->mobilenumber }}" class="form-control" readonly>
					</div>					
					
				</div>
			</div>

			<div class="form-group">
				<div class="row">
					<div class="col-md-6">
						<label><b>Email</b></label>
						<input type="text" value="{{ $getStaff->email }}"  class="form-control" readonly>
					</div>
					
					<div class="col-md-6">
						<label><b>Role</b></label>
						<input type="text" value="{{ $getStaff->usergroup }}" class="form-control" readonly>
					</div>
				</div>
			</div>

			<div class="form-group">
				<div class="row">					
					<div class="col-md-6">
						<label><b>Status</b></label>
						<input type="text" value="{{ $getStaff->status }}" class="form-control" readonly>
					</div>
					<div class="col-lg-6">
						<label><b>Profile Image</b></label>
						<input type="file" id="picture" name="picture" data-show-upload="false" class="file-input" readonly>
						<span class="help-block"> Upload only <code>Jpeg</code> | <code>Jpg</code> | <code>Png</code>  file extensions.</span>
					</div>
					
				</div>
			</div>
			 
				
							<br/><br/><br/>
		                        <div class="text-right">
		                        	<button type="submit" class="btn btn-rounded btn-primary">Save <i class="icon-arrow-right14 position-right"></i></button>
		                        </div>
							</form>
						</div>
					</div>								

				</div>					


							</div>

						</div>
					</div>
						 @include('cytonnusers.footer')
				</div>
				<!-- /content area -->
			</div>
		</div>

	@endsection
