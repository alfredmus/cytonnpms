@extends('admin.header')

@section('content')
<div class="login-container">
<div class="page-container">
        <div class="page-content">
            <div class="content-wrapper">
                <div class="content">                    
                    <form  method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="panel panel-body login-form">
                            <div class="text-center">
                                <div>
                                <a href="#"><img src="{{asset('assets/images/placeholdernew.svg')}}" style="width: 110px; height: 210px; margin-left: 10px;" alt=""></a>
                                </div>
                                <h5 class="content-group" style="color: darkgreen">Login to your account <small class="display-block"></small></h5>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="form-group has-feedback has-feedback-left">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Enter Email" required autofocus>

                                <div class="form-control-feedback">
                                    <i class="icon-user text-muted"></i>
                                </div>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
              

                         <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">

                            <div class="form-group has-feedback has-feedback-left">
                                <input id="password" type="password" class="form-control" name="password" placeholder="Enter password" required>

                                <div class="form-control-feedback">
                                    <i class="icon-lock2 text-muted"></i>
                                </div>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                            <div class="form-group login-options">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label class="checkbox-inline" style="color: darkgreen">
                                            <input type="checkbox" class="styled" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                            Remember Me
                                        </label>
                                    </div>

                                    <div class="col-sm-6 text-right">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn bg-success-400 btn-block">Login <i class="icon-arrow-right14 position-right"></i>
                                </button>
                            </div>
 
                        </div>
                    </form>

                </div>

            </div>

        </div>

    </div>
</div>
<script type="text/javascript">


$(".btn-refresh").click(function(){
  $.ajax({
     type:'GET',
     url:'/refresh_captcha',
     success:function(data){
        $(".captcha span").html(data.captcha);
     }
  });
});


</script>
@endsection
