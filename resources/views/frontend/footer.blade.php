

		<div class="row" style="background: #333333; bottom: 0; left: 0; width:102%;">
			<div class="row copyright" style="margin-left:25px; margin-right: 25px;">
				<div class="col-xs-12 col-sm-5">
					<div class="copyright-info">
						<span>Copyright © 2017-2018 Kenbright. All Rights Reserved.Powered by <a href="https://flag42.com/" target="blank">© Flag42</a> </span>
					</div>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-push-1">
					<div class="footer-menu">
						<ul class="nav unstyled">
							<li><a href="about.html">About</a></li>
							<li><a href="index.html">© Kenbright</a></li>
							<li><a href="privacy-policy.html">Legal</a></li>
							<li><a href="privacy-policy.html">Privacy Statement</a>
						</ul>
					</div>
				</div>
			</div> 
		</div>
	<!--linkedin insight tags-->
	<script type="text/javascript">
		_linkedin_data_partner_id = "98802";					
	</script>
	<script type="text/javascript">
					(function(){var s = document.getElementsByTagName("script")[0];
					var b = document.createElement("script");
					b.type = "text/javascript";b.async = true;
					b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
					s.parentNode.insertBefore(b, s);})();
	</script>
	<noscript>
	<img height="1" width="1" style="display:none;" alt="" src="https://dc.ads.linkedin.com/collect/?pid=98802&fmt=gif" />
	</noscript>

	<!-- Javascript Files
	================================================== -->

	<!-- initialize jQuery Library -->
	<script type="text/javascript" src="{{asset('client-js/jquery.js')}}"></script>
	<!-- Bootstrap jQuery -->
	<script type="text/javascript" src="{{asset('client-js/bootstrap.min.js')}}"></script>
	<!-- Owl Carousel -->
	<script type="text/javascript" src="{{asset('client-js/owl.carousel.min.js')}}"></script>
	<!-- Counter -->
	<script type="text/javascript" src="{{asset('client-js/jquery.counterup.min.js')}}"></script>
	<!-- Waypoints -->
	<script type="text/javascript" src="{{asset('client-js/waypoints.min.js')}}"></script>
	<!-- Color box -->
	<script type="text/javascript" src="{{asset('client-js/jquery.colorbox.js')}}"></script>
	<!-- Isotope -->
	<script type="text/javascript" src="{{asset('client-js/isotope.js')}}"></script>
	<script type="text/javascript" src="{{asset('client-js/ini.isotope.js')}}"></script>
	<!-- Google Map API Key Source -->
	<script src="{{asset('http://maps.google.com/maps/api/js?sensor=false')}}"></script>
	<!-- For Google Map -->
	<!-- Doc http://www.mkyong.com/google-maps/google-maps-api-hello-world-example/ -->
	<script type="text/javascript" src="{{asset('client-js/gmap3.min.js')}}"></script>
	<!-- Template custom -->
	<script type="text/javascript" src="{{asset('client-js/custom.js')}}"></script>
	<!-- engine custom -->
	<script type="text/javascript" src="{{asset('client-js/kcust.js')}}"></script>
	<!-- mailchimp script start -->
	<!-- <script type='text/javascript' src="{{asset('//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js')}}"></script> -->
	<script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!-- mailchimp script -->
	
	</div>
</body>
</html>