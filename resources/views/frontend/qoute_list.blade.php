@extends('frontend.frontend')
@section('content')
<div class="container">
<div class="row">
	<div class="col col-sm-12" style="padding-left:20px">
		<div class="col col-sm-3">
		<h3>NAME and LOGO</h3>
		</div>
		<div class="col col-sm-3">
		<h3>Policy Benefits</h3>
		</div>
		<div class="col col-sm-3">
		<h3>Premium Value</h3>
		</div>
		<div class="col col-sm-3">
		<h3>Actions</h3>
		</div>
	</div>


	<div class="row align-content-center">
		<div class="col col-sm-3">
			<img src="{{url('http://kenbright.co.ke/images/logo.png')}}">
			<br/><span><h4>Kenbright Holdings Limited</h4></span>

		</div>
		<div class="col col-sm-3">
			<div class="btn btn-rounded btn-success text-centre">Wind Screen </br><span>50,000</span> </div>
			<div class="btn btn-rounded btn-success text-centre">Wind Screen </br><span>50,000</span> </div>
			<div class="btn btn-rounded btn-success text-centre">Wind Screen </br><span>50,000</span> </div>
			<div class="btn btn-rounded btn-success text-centre">Wind Screen </br><span>50,000</span> </div>
			
			<!--<div class="row">

				 <div class="btn btn-rounded button-cyan"><h6>Benefits</h6></div><div class="col col-sm-5"><h6>Values</h6></div>
				<div class="col col-sm-7">Windscreen cover</div><div class="col col-sm-5">50,000</div>
				<div class="col col-sm-7">Windscreen cover</div><div class="col col-sm-5">50,000</div>
				<div class="col col-sm-7">Windscreen cover</div><div class="col col-sm-5">50,000</div>
				<div class="col col-sm-7">Windscreen cover</div><div class="col col-sm-5">50,000</div>
				<div class="col col-sm-7">Windscreen cover</div><div class="col col-sm-5">50,000</div> 
			</div>-->
		</div>
		<div class="col col-sm-3"><h4>48,000</h4></div>
		<div class="col col-sm-3">
			<a class="btn btn-info">Buy Policy  &nbsp;&nbsp;<i class="fa fa-credit-card"></i></a> &nbsp;
			<a href="#colap" class="btn btn-success" data-toggle="collapse">Quote Details &nbsp;&nbsp; <i class="fa fa-search"></i></a>
		  
		</div>
    </div>
   <div class="collapse" id="colap">
	<div class="row mydrawable">
		<div clas="row">
			<div class="col col-sm-4 text-center">
				<img src="{{url('http://kenbright.co.ke/images/logo.png')}}">
			</div>
			<div class="col col-sm-6 text-center">
				<h3>Kenbright Holdings Brokers</h3>
			</div>
			<div class="col col-sm-2 text-right">
				<h3><i class="fa fa-times"></i></h3>
			</div>
		</div>
		<div clas="row">
			<div class="col col-sm-12 text-center">
				<span>
					This is the description of of the policy quoted above with all its included benefit.
				</span>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12 text-center">
				<h5>BENEFITS</h5>
			</div>
			<div class="col-sm-10 text-centre">
			<table class="table table-stripped" >
				<thead>
					<tr>
					<th scope="column">#</th>
					<th scope="column"><h5>Benefit</h5></th>
					<th scope="column"><h5>Amount</h5></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">1</th>
						<td>Wind screen Damage</td>
						<td>50,000</td>
					</tr>
					<tr scope="row">
						<th scope="row">2</th>
						<td>Wind screen Damage</td>
						<td>50,000</td>
					</tr>
					<tr scope="row">
						<th scope="row">3</th>
						<td>Wind screen Damage</td>
						<td>50,000</td>
					</tr>
					<tr scope="row">
						<th scope="row">4</th>
						<td>Wind screen Damage</td>
						<td>50,000</td>
					</tr>
					<tr scope="row">
						<th scope="row">5</th>
						<td>Wind screen Damage</td>
						<td>50,000</td>
					</tr>
				</tbody>
			</table>
		  </div>
		</div>
		<div class="row" >
			<div class="col-sm-12 text-center">
				<h5>EXCESS</h5>
			</div>
			<div class="col-sm-10 text-centre">
			<table class="table table-dark" >
				<thead>
					<tr>
					<th scope="column">#</th>
					<th scope="column"><h5>Benefit</h5></th>
					<th scope="column"><h5>Amount</h5></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row">1</th>
						<td>Wind screen Damage</td>
						<td>50,000</td>
					</tr>
					<tr scope="row">
						<th scope="row">2</th>
						<td>Wind screen Damage</td>
						<td>50,000</td>
					</tr>
					<tr scope="row">
						<th scope="row">3</th>
						<td>Wind screen Damage</td>
						<td>50,000</td>
					</tr>
					<tr scope="row">
						<th scope="row">4</th>
						<td>Wind screen Damage</td>
						<td>50,000</td>
					</tr>
					<tr scope="row">
						<th scope="row">5</th>
						<td>Wind screen Damage</td>
						<td>50,000</td>
					</tr>
				</tbody>
			</table>
		  </div>
		</div>
		<div class="row">
          	<div class="col-sm-12 text-center">
				<h5>ADDITIONAL BENEFITS</h5>
			</div>
			<div class="col-sm-8">
				<table class="table">
					<thead>
						<tr>
							<th scope="colum">Benefit</th>
							<th scope="colum">Value</th>
							<th scope="colum">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Benefit</td>
							<td>130</td>
							<td><input type="checkbox" name=""></td>
						</tr>					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
				<div class="col-sm-12 text-right">
					<hr/>
					<h1 style="margin-right:20px"><i>Total:</i> &nbsp;&nbsp;&nbsp;48,000</h1>
					<hr/>
				</div>
		</div>
	</div>
   </div>
	<!-- End of Collapsible-->
</div>
</div>

@endsection