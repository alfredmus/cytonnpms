@extends('frontend.frontend')
@section('content')
<div class="row">
<div class="col col-md-12 text-center"><H1>SELECT A PRODUCT</H1></div>
	<div class="col-md-4">
		<div class="ts-service-icon-wrapper">
			<a href="{{url('details')}}"><span class="ts-service-icon"><i class="fa fa-car"></i></span>
				<H3>Buy Motor Insurance</H3>
			</a>
		</div>
	</div>
	<div class="col-md-4">
		<div class="ts-service-icon-wrapper">
			<a href="#"><span class="ts-service-icon"><i class="fa fa-money"></i></span></a>
			<H3>File CLaim</H3>
		</div>
	</div>
	<div class="col-md-4">
		<div class="ts-service-icon-wrapper">
			<a href="#"><span class="ts-service-icon"><i class="fa fa-bar-chart"></i></span></a>
			<H3>View Claim</H3>
		</div>
	</div>
	<div class="col-md-4">
		<div class="ts-service-icon-wrapper">
			<a href="#"><span class="ts-service-icon"><i class="fa fa-wrench"></i></span></a>
			<H3>View Garages</H3>
		</div>
	</div>
	<div class="col-md-4">
		<div class="ts-service-icon-wrapper">
			<a href="#"><span class="ts-service-icon"><i class="fa fa-ship"></i></span></a>
			<H3>Order Spare Parts</H3>
		</div>
	</div>
</div>
@endsection
