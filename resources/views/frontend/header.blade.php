<!DOCTYPE html>
<html lang="en">
<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
    <title> KENBRIGHT | IBS </title>

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<!-- Favicon
	================================================== -->
	<link rel="shortcut icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">
	<link rel="icon" href="{{asset('/favicon.ico')}}" type="image/x-icon">

	<!-- Favicons
	================================================== -->
	<!-- <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/favicon/favicon-144x144.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/favicon/favicon-72x72.png">
	<link rel="apple-touch-icon-precomposed" href="img/favicon/favicon-54x54.png"> -->
	<!-- mailchimp -->
	<link href="{{asset('//cdn-images.mailchimp.com/embedcode/classic-10_7.css')}}" rel="stylesheet" type="text/css">
	
	<!-- CSS
	================================================== -->
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="{{asset('client-css/bootstrap.min.css')}}">
	<!-- Template styles-->
	<link rel="stylesheet" href="{{asset('client-css/style.css')}}">
	<!-- Responsive styles-->
	<link rel="stylesheet" href="{{asset('client-css/responsive.css')}}">
	<!-- FontAwesome -->
	<!-- https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css -->
	<!-- <link rel="stylesheet" href="{{asset('client-css/font-awesome.min.css')}}"> -->
	<link rel="stylesheet" href="{{asset('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')}}">
	<!-- Animation -->
	<link rel="stylesheet" href="{{asset('client-css/animate.css')}}">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{{asset('client-css/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('client-css/owl.theme.cs')}}s">
	<!-- Colorbox -->
	<link rel="stylesheet" href="{{asset('client-css/colorbox.css')}}">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111886634-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111886634-1');
</script>
<!-- end of Global site tag (gtag.js) - Google Analytics -->
</head>
<body>

	<div class="body-inner">

			<div id="top-bar" class="top-bar">
				<div class="container">
					<div class="row">
						
						<div class="col-md-6 col-sm-6 col-xs-12 top-social">
							<ul class="unstyled">
								<li>
									<a title="Facebook" href="https://www.facebook.com/kenbrightinsurancebrokers/">
										<span class="social-icon"><i class="fa fa-facebook"></i></span>
									</a>
									<a title="Twitter" href="https://twitter.com/kenbright_ke ">
										<span class="social-icon"><i class="fa fa-twitter"></i></span>
									</a>
									<!-- <a title="Google+" href="#">
										<span class="social-icon"><i class="fa fa-google-plus"></i></span>
									</a> -->
									<a title="Linkdin" href="https://www.linkedin.com/company/10385091/">
										<span class="social-icon"><i class="fa fa-linkedin"></i></span>
									</a>
									<a title="instagram" href="https://www.instagram.com/kenbright_ke/">
										<span class="social-icon"><i class="fa fa-instagram"></i></span>
									</a>
								</li>
							</ul>
						</div><!--/ Top social end -->

						<div class="col-md-6 col-sm-6 col-xs-12 top-menu ">
							<ul class="unstyled">
								<!-- <li><a href="healthcare.html" data-toggle="modal" data-target="#myModal">Claim &amp; Policy</a></li> -->
							 	<li><a href="makeaclaim.html" data-toggle="modal" data-target="#makeaclaim">Make a Claim</a></li>
								<!-- <li><a href="#">Track Your Claim</a></li> -->
							</ul>
						</div><!--/ Top menu end -->

					</div><!--/ Content row end -->
				</div><!--/ Container end -->
			</div><!--/ Topbar end -->
			<!-- make a claim modal container -->
		<div id="makeaclaim" class="modal fade">
		    <div class="modal-dialog">
		      <div class="modal-content">
		      </div>
		    </div>
		  </div>
		  <!-- make a claim modal container end -->
			<!-- Header start -->
			<header id="header" class="header">
				<div class="container">
					<div class="row">
						<div class="logo col-xs-12 col-sm-3">
		                <a href="index.html">
		                	<img src="clients-images/logo.png" alt="kenbright logo">
		                </a>
		         	</div><!-- logo end -->

		         	<div class="col-xs-12 col-sm-7 header-right">
		         		<ul class="top-info">
								<li>
									<div class="info-box"><span class="info-icon"><i class="fa fa-map-marker">&nbsp;</i></span>
										<div class="info-box-content">
										<p class="info-box-title">Ground Floor, ACK Garden House</p>
											<p class="info-box-subtitle">1st Ngong Avenue, Upperhill. NAIROBI</p>
											<!-- <p class="info-box-subtitle">Nairobi, Kenya</p> -->
										</div>
									</div>
								</li>
								<li>
									<div class="info-box"><span class="info-icon"><i class="fa fa-phone">&nbsp;</i></span>
										<div class="info-box-content">
											<p class="info-box-title">(+254) 722 784 434</p>
											<p class="info-box-subtitle"><a href="mailto:info@kenbright.co.ke">info@kenbright.co.ke</a></p>
										</div>
									</div>
								</li>
								<li>
									<div class="info-box"><span class="info-icon"><i class="fa fa-compass">&nbsp;</i></span>
										<div class="info-box-content">
											<p class="info-box-title">08.00 - 05.00</p>
											<p class="info-box-subtitle">Monday to Friday</p>
										</div>
									</div>
								</li>
							</ul>
		         	</div><!-- header right end -->
					</div><!-- Row end -->
				</div><!-- Container end -->
			</header><!--/ Header end -->

			<!-- <nav class="site-navigation navigation">
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
							<div class="site-nav-inner pull-left">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						        	<span class="sr-only">Toggle navigation</span>
						        	<span class="icon-bar"></span>
						        	<span class="icon-bar"></span>
						        	<span class="icon-bar"></span>
				    			</button>

				    			<div class="collapse navbar-collapse navbar-responsive-collapse">
				    				<ul class="nav navbar-nav">
										<li class="dropdown active">
					                  <a href="index.html" >Home</a>
					               </li>
					                <li class="dropdown">
					                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Insurance Brokerage <i class="fa fa-angle-down"></i></a>
										<ul class="dropdown-menu" role="menu">
			                           <li><a href="personal-lines.html">Personal Lines</a></li>
			                           <li><a href="commercial-lines.html">Commercial Lines</a></li></ul>
					               </li>
					               	<li>
					                  <a href="healthcare.html" class="active">Healthcare</a>
					               </li>
				                   	<li>
					                  <a href="kafs.html" class="">Actuarial Services</a>
					               </li>
					               
					               <li class="dropdown">
					                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">About<i class="fa fa-angle-down"></i></a>
									<ul class="dropdown-menu" role="menu">
			                      <li><a href="about.html">About Us</a></li>
			                 <li><a href="why-us.html">Why Kenbright</a></li>
			                      <li><a href="offer.html">We Offer</a></li>
			                    <li><a href="team.html">Team Members</a></li> -->
			               <!-- <li><a href="reviews.html">Client Reviews</a></li> -->
			                          <!-- <li>
		                         	   <a href="career.html">Career</a> -->
		<!--                          			<ul class="dropdown-menu">
		                         				<li><a href="career.html">Job List</a></li>
		                         			</ul> -->
		                         		<!-- </li>
			                    <li><a href="faq.html">Faq</a></li>
			                </ul> -->
			<!-- <li class="dropdown"><a href="news.html">News </a></li> -->
					<!-- <li><a href="contact.html" >Contact</a></li>

					            </ul><!--/ Nav ul end -->
				    			<!-- </div> --><!--/ Collapse end -->

							<!-- </div> --><!-- Site Navbar inner end -->
<!-- 
							<div class="find-agent pull-right">
								<a href="http://getmint.co.ke/">
								<img src="clients-images/icons/mint160.jpg" alt="mint logo">
				    			</a>
				    		</div> -->

				    	<!-- </div> --><!--/ Col end -->
					<!-- </div> --><!--/ Row end -->
				<!-- </div> --><!--/ Container end -->
			<!-- </nav> --><!--/ Navigation end -->
			