@extends('frontend.frontend')
@section('content')
<div style="height:400px; padding:5px; "">
	<form>
	<div style="display: inline;">
		<H2>What is the current value of your car?
			<input type="number" name="value" id="value" onkeyup="strtquote()"></H2>
         <span id="valueSpan"></span>
	</div>
	<div id="iduse" style="visibility: hidden;">
		<H2>What is the use of your vehicle
			<select name="usage" id="usage" onchange="act2()">
				<option selected="true" disabled="true"></option>
				<option>Private</option>
				<option>Public</option>
				<option>Commercial</option>
			</select>
			</H2>
	</div>
	<div id="idmake" style="visibility: hidden;">
		<H2>What make is your car?
			<select name="make" id="make" onchange="act3()">
			<option selected="true" disabled="true"></option>
			<option>Toyota</option>
			<option>Nissan</option>
			<option>Audi</option>
			<option>Volks Wagen</option>
			<option>Mercedes</option>
			<option>Mitsubishi</option>
			<option>Jeep</option> 
			</select></H2>
	</div>
	<div style="visibility: hidden;" id="idyear">
		<H2>What is your cars year of manufacture?
			<select name="year" id="year" onchange="act4()">
				<option selected="true" disabled="true"></option>
				<option>2018</option>
				<option>2017</option>
				<option>2016</option>
				<option>2015</option>
				<option>2014</option>
				<option>2013</option>
				<option>2012</option>
				<option>2011</option>
				<option>2010</option>
				<option>2009</option>
				<option>2008</option>
				<option>2007</option>
				<option>2006</option>
			</select>
			</H2>
	</div>
	<div style="visibility: hidden;" id="idstart">
		<H2>When would you like your cover to start?
			<!-- <input type="text" name="value" id="time"></H2> -->
			<select name="year" id="start" onchange="act5()">
				<option selected="true" disabled="true"></option>
				<option>2018</option>
				<option>2017</option>
				<option>2016</option>
				<option>2015</option>
				<option>2014</option>
				<option>2013</option>
				<option>2012</option>
				<option>2011</option>
				<option>2010</option>
				<option>2009</option>
				<option>2008</option>
				<option>2007</option>
				<option>2006</option>
			</select>
		</H2>
	</div>
	</form>
</div>
@endsection
@section('footer')

<script type="text/javascript">
	
</script>
@stack('script');
@endsection
